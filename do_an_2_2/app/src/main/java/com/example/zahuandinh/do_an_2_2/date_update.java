package com.example.zahuandinh.do_an_2_2;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;

public class date_update {
    int tt = 0, aa, vitri_batdau, vitri_ketthuc, vitri, yy,mmm,dd, b = 0,stt=0;
    int soluong =6, check_number, total_number,i = 0, phongkham, capnhat_soluong, schedule_soluong;
    String doctor, sophong, node="", date_time="", node_date="",date_esp="";
    DatabaseReference mData = FirebaseDatabase.getInstance().getReference();
    public void update()
    {
        //TÍNH NGÀY HIỆN TẠI
        Calendar calendar = Calendar.getInstance();
        final int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        month = month + 1;
        final int day = calendar.get(Calendar.DAY_OF_MONTH);
        //TẠO CHUỖI NGÀY HIỆN TẠI
        if(day < 10 && month < 10)
        {
            date_esp = "0" + String.valueOf(day) + "-" + "0" + String.valueOf(month) + "-" + String.valueOf(year);
        }
        else if(day < 10)
        {
            date_esp = "0" + String.valueOf(day) + "-" + String.valueOf(month) + "-" + String.valueOf(year);
        }
        else
        {
            date_esp = String.valueOf(day) + "-" + "0" + String.valueOf(month) + "-" + String.valueOf(year);
        }
        date_time = String.valueOf(day) + "-" + String.valueOf(month) + "-" + String.valueOf(year);
        // Kiểm tra ngày hiện tại với ngày lưu trên Firebase để cập nhật dữ liệu
        final int finalMonth = month;
        mData.child("date").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //Lấy ngày lưu trên Firebase
                node_date = dataSnapshot.getValue().toString();
                vitri_batdau = node_date.indexOf("-");
                dd = Integer.valueOf(node_date.substring(0,vitri_batdau));
                vitri_ketthuc = node_date.lastIndexOf("-");
                mmm = Integer.valueOf(node_date.substring(vitri_batdau+1,vitri_ketthuc));
                yy = Integer.valueOf(node_date.substring(vitri_ketthuc+1, vitri_ketthuc+5));
                mData.child("ROOM").child("soluong").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        soluong = ((Long) dataSnapshot.getValue()).intValue();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                //Kiểm tra ngày tháng năm hiện tại xem có khớp với firebase ko, nếu khớp --> đã cập nhật , nếu không --> chưa cập nhật
                if(yy != year || mmm != finalMonth || dd != day)
                {
                    Log.d("tui_la_huan","CẦN CẬP NHẬT LẠI DỮ LIỆU...........");
                    //THIẾT LẬP LẠI MỐC THỜI GIAN KHÁM VỀ GIỜ BẮT ĐẦU KHÁM
                    mData.child("ROOM").child("1").child("w_hour").setValue(6);
                    mData.child("ROOM").child("1").child("w_minute").setValue(0);
                    mData.child("ROOM").child("1").child("w_shift").setValue(0);
                    mData.child("ROOM").child("1").child("w_checknum").setValue(0);
                    mData.child("ROOM").child("1").child("flag").setValue(0);
                    mData.child("ROOM").child("1").child("doctor").setValue("No doctor in the room");
                    mData.child("ROOM").child("2").child("w_hour").setValue(6);
                    mData.child("ROOM").child("2").child("w_minute").setValue(0);
                    mData.child("ROOM").child("2").child("w_shift").setValue(0);
                    mData.child("ROOM").child("2").child("w_checknum").setValue(0);
                    mData.child("ROOM").child("2").child("flag").setValue(0);
                    mData.child("ROOM").child("2").child("doctor").setValue("No doctor in the room");
                    mData.child("ROOM").child("3").child("w_hour").setValue(6);
                    mData.child("ROOM").child("3").child("w_minute").setValue(0);
                    mData.child("ROOM").child("3").child("w_shift").setValue(0);
                    mData.child("ROOM").child("3").child("w_checknum").setValue(0);
                    mData.child("ROOM").child("3").child("flag").setValue(0);
                    mData.child("ROOM").child("3").child("doctor").setValue("No doctor in the room");
                    mData.child("ROOM").child("4").child("w_hour").setValue(6);
                    mData.child("ROOM").child("4").child("w_minute").setValue(0);
                    mData.child("ROOM").child("4").child("w_shift").setValue(0);
                    mData.child("ROOM").child("4").child("w_checknum").setValue(0);
                    mData.child("ROOM").child("4").child("flag").setValue(0);
                    mData.child("ROOM").child("4").child("doctor").setValue("No doctor in the room");
                    mData.child("ROOM").child("5").child("w_hour").setValue(6);
                    mData.child("ROOM").child("5").child("w_minute").setValue(0);
                    mData.child("ROOM").child("5").child("w_shift").setValue(0);
                    mData.child("ROOM").child("5").child("w_checknum").setValue(0);
                    mData.child("ROOM").child("5").child("flag").setValue(0);
                    mData.child("ROOM").child("5").child("doctor").setValue("No doctor in the room");
                    mData.child("ROOM").child("6").child("w_hour").setValue(6);
                    mData.child("ROOM").child("6").child("w_minute").setValue(0);
                    mData.child("ROOM").child("6").child("w_shift").setValue(0);
                    mData.child("ROOM").child("6").child("w_checknum").setValue(0);
                    mData.child("ROOM").child("6").child("flag").setValue(0);
                    mData.child("ROOM").child("6").child("doctor").setValue("No doctor in the room");
                    mData.child("ROOM").child("7").child("w_hour").setValue(6);
                    mData.child("ROOM").child("7").child("w_minute").setValue(0);
                    mData.child("ROOM").child("7").child("w_shift").setValue(0);
                    mData.child("ROOM").child("7").child("w_checknum").setValue(0);
                    mData.child("ROOM").child("7").child("flag").setValue(0);
                    mData.child("ROOM").child("7").child("doctor").setValue("No doctor in the room");
                    //KIỂM TRA TRONG SCHEDULE CÓ BAO NHIÊU NGÀY ĐC ĐẶT, NẾU = 0 THÌ KHÔNG XÉT, NẾU KHÁC KHÔNG THÌ XỬ LÝ
                    mData.child("SCHEDULE").child("soluong").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            schedule_soluong = ((Long) dataSnapshot.getValue()).intValue();
                            if(schedule_soluong > 0)
                            {
                                mData.child("SCHEDULE").addChildEventListener(new ChildEventListener() {
                                    @Override
                                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                                        //Nếu ngày hôm nay có bênh nhân đặt lịch thì xử lý
                                        if(schedule_soluong > 0)
                                        {
                                            vitri_batdau = dataSnapshot.getValue().toString().indexOf(date_time);
                                            if(vitri_batdau > 0)
                                            {
                                                //NẾU TRONG DANH SÁCH ĐẶT LICH CÓ NGÀY HÔM NAY THÌ CHO STT = 1 VÀ B = SCHEDULE ĐỂ THOÁT VIỆC KIỂM TRA
                                                stt = 1;
                                                b = schedule_soluong;
                                            }
                                            if(vitri_batdau < 0)
                                            {
                                                //NẾU TRONG DS ĐẶT LỊCH KHÔNG CÓ NGÀY HÔM NAY THÌ TĂNG BIẾN B LÊN 1 ĐỂ KIỂM TRA TIẾP
                                                b++;
                                            }
                                        }
                                        //QUÁ TRÌNH KIỂM TRA SẼ KẾT THÚC KHI
                                        if(b == schedule_soluong)
                                        {
                                            if(stt == 1)
                                            {
                                                Log.d("dinhhuanoi","HÔM NAY CÓ BỆNH NHÂN ĐẶT LỊCH");
                                                //ĐỌC SỐ LƯỢNG PHÒNG ĐÃ ĐƯỢC ĐẶT TRONG NGÀY HÔM NAY
                                                mData.child("SCHEDULE").child(date_time).child("soluong").addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                        Log.d("COMEBACK","NODE* :  " + dataSnapshot.getValue().toString());
                                                        if(dataSnapshot.exists())
                                                        {
                                                            Log.d("COMEBACK","NODE :  " + dataSnapshot.getValue().toString());
                                                            capnhat_soluong = ((Long) dataSnapshot.getValue()).intValue();
                                                            for(int i = 1; i < (capnhat_soluong+1); i++)
                                                            {
                                                                final int finalI = i;
                                                                mData.child("SCHEDULE").child(date_time).child(String.valueOf(i)).child("total_number").addListenerForSingleValueEvent(new ValueEventListener() {
                                                                    @Override
                                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                        Log.d("tui_la_huan","i   :  "+ finalI);
                                                                        Log.d("tui_la_huan","kt   :  "+dataSnapshot.getValue().toString());
                                                                        mData.child("ROOM").child(String.valueOf(finalI)).child("total_number").setValue(((Long) dataSnapshot.getValue()).intValue());
                                                                        Log.d("tui_la_huan",dataSnapshot.getValue().toString());
                                                                        mData.child("ROOM").child(String.valueOf(finalI)).child("check_number").setValue(0);
                                                                        mData.child("ROOM").child("nextroom").setValue(1);
                                                                        mData.child("date").setValue(date_time);
                                                                    }

                                                                    @Override
                                                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                                                    }
                                                                });
                                                            }
                                                            for(int i = (capnhat_soluong+1); i < (soluong+1); i++)
                                                            {
                                                                mData.child("ROOM").child(String.valueOf(i)).child("total_number").setValue(0);
                                                                mData.child("ROOM").child(String.valueOf(i)).child("check_number").setValue(0);
                                                            }
                                                        }
                                                    }

                                                    @Override
                                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                                    }
                                                });
                                            }
                                            if (stt == 0)
                                            {
                                                for(int i = 1; i < (soluong+1); i ++)
                                                {
                                                    mData.child("ROOM").child(String.valueOf(i)).child("check_number").setValue(0);
                                                    mData.child("ROOM").child(String.valueOf(i)).child("total_number").setValue(0);
                                                    Log.d("dinhhuanoi","ROOM" + i + " - total_number: 0");
                                                    mData.child("date").setValue(date_time);
                                                    mData.child("ROOM").child("nextroom").setValue(1);
                                                }
                                            }
                                        }
                                    }

                                    @Override
                                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                                    }

                                    @Override
                                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                                    }

                                    @Override
                                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            }
                            if(schedule_soluong == 0)
                            {
                                Log.d("dinhhuanoi","soluong: " + soluong);
                                for(int i = 1; i < (soluong+1); i ++)
                                {
                                    mData.child("ROOM").child(String.valueOf(i)).child("check_number").setValue(0);
                                    mData.child("ROOM").child(String.valueOf(i)).child("total_number").setValue(0);
                                    Log.d("dinhhuanoi","ROOM" + i + " - total_number: 0");
                                    mData.child("date").setValue(date_time);
                                    mData.child("ROOM").child("nextroom").setValue(1);
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
                else
                {
                    Log.d("dinhhuanoi","THỜI GIAN GIỐNG NHAU");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        mData.child("ROOM").child("soluong").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                soluong = ((Long)dataSnapshot.getValue()).intValue();

                mData.child("ROOM").addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        node = dataSnapshot.getValue().toString();
                        if(node.length() > 5)
                        {
                            //Biến str_temp làm biến tạm dùng để tách chuỗi cần thiết để hiển thị những thông tin về phòng khám
                            String str_temp="";
                            tt++;
                            vitri_batdau = node.indexOf("sophong=");
                            str_temp = node.substring(vitri_batdau);
                            vitri_ketthuc = str_temp.indexOf(",");
                            if(vitri_ketthuc == -1)
                            {
                                vitri_ketthuc = str_temp.indexOf("}");
                            }
                            sophong =str_temp.substring(8,vitri_ketthuc);

                            vitri_batdau = node.indexOf("total_number=");
                            str_temp = node.substring(vitri_batdau);
                            vitri_ketthuc = str_temp.indexOf(",");
                            if(vitri_ketthuc == -1)
                            {
                                vitri_ketthuc = str_temp.indexOf("}");
                            }
                            total_number = Integer.valueOf(str_temp.substring(13,vitri_ketthuc)) ;

                            vitri_batdau = node.indexOf("check_number=");
                            str_temp = node.substring(vitri_batdau);
                            vitri_ketthuc = str_temp.indexOf(",");
                            if(vitri_ketthuc == -1)
                            {
                                vitri_ketthuc = str_temp.indexOf("}");
                            }
                            check_number =Integer.valueOf(str_temp.substring(13,vitri_ketthuc));

                            vitri_batdau = node.indexOf("doctor=");
                            str_temp = node.substring(vitri_batdau);
                            vitri_ketthuc = str_temp.indexOf(",");
                            if(vitri_ketthuc == -1)
                            {
                                vitri_ketthuc = str_temp.indexOf("}");
                            }
                            doctor =str_temp.substring(7,vitri_ketthuc);
                        }
                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        node = dataSnapshot.getValue().toString();
                        if(node.length() > 5)
                        {
                            String str_temp="";
                            //Cắt số phòng
                            vitri_batdau = node.indexOf("sophong=");
                            str_temp = node.substring(vitri_batdau);
                            vitri_ketthuc = str_temp.indexOf(",");
                            if(vitri_ketthuc == -1)
                            {
                                vitri_ketthuc = str_temp.indexOf("}");
                            }
                            sophong =str_temp.substring(8,vitri_ketthuc);
                            // Cắt total_number
                            vitri_batdau = node.indexOf("total_number=");
                            str_temp = node.substring(vitri_batdau);
                            vitri_ketthuc = str_temp.indexOf(",");
                            if(vitri_ketthuc == -1)
                            {
                                vitri_ketthuc = str_temp.indexOf("}");
                            }
                            total_number = Integer.valueOf(str_temp.substring(13,vitri_ketthuc)) ;
                            // Cắt Check_number
                            vitri_batdau = node.indexOf("check_number=");
                            str_temp = node.substring(vitri_batdau);
                            vitri_ketthuc = str_temp.indexOf(",");
                            if(vitri_ketthuc == -1)
                            {
                                vitri_ketthuc = str_temp.indexOf("}");
                            }
                            check_number =Integer.valueOf(str_temp.substring(13,vitri_ketthuc));
                            // Cắt Doctor
                            vitri_batdau = node.indexOf("doctor=");
                            str_temp = node.substring(vitri_batdau);
                            vitri_ketthuc = str_temp.indexOf(",");
                            if(vitri_ketthuc == -1)
                            {
                                vitri_ketthuc = str_temp.indexOf("}");
                            }
                            doctor =str_temp.substring(7,vitri_ketthuc);
                            vitri = Integer.valueOf(sophong.substring(2,3));
                        }
                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
