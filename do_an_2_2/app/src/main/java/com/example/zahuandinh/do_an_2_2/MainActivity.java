package com.example.zahuandinh.do_an_2_2;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.integration.android.IntentIntegrator;

public class MainActivity extends AppCompatActivity {
EditText account_main, password_main, reset_mail, reset_id;
ImageView signin_main, signup_main, qr_main, forget_main;
Button reset_ok;
Intent intent;
//thêm ngày 3/1
room_shownumber room;
int vitri_batdau, vitri_ketthuc;
String node = "";
FirebaseAuth mAuth;
DatabaseReference mData;
Dialog dialog;
String email_si;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        anhxa();
        final ProgressBar simpleProgressBar = (ProgressBar) findViewById(R.id.simpleProgressBar);
        mAuth = FirebaseAuth.getInstance();
        mData = FirebaseDatabase.getInstance().getReference();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            // VÔ HIỆU HÓA CÁC NÚT NHẤN KHI ĐÃ CÓ NGƯỜI ĐĂNG NHẬP
            account_main.setEnabled(false);
            password_main.setEnabled(false);
            signup_main.setEnabled(false);
            signin_main.setEnabled(false);
            forget_main.setEnabled(false);
            // LẤY THÔNG TIN NGƯỜI DÙNG
            String name = user.getDisplayName();
            email_si = user.getEmail();
            String uid = user.getUid();
            String photo = user.getPhotoUrl().toString();
            Log.d("MainActivity", "\n" + name + "\n" + email_si + "\n" + uid );
            simpleProgressBar.setVisibility(View.VISIBLE);
            Log.d("XU LY", "PS1" );
            Toast.makeText(MainActivity.this, "Log In Successfully",Toast.LENGTH_SHORT).show();
            vitri_batdau = email_si.indexOf("@");
            node = node + email_si.substring(0,vitri_batdau);
            vitri_ketthuc = email_si.indexOf(".com");
            node = node + email_si.substring(vitri_batdau + 1,vitri_ketthuc);
            Log.d("MainActivity", "NODE : " + node );
            mData.child("USERS").child(node).child("quyen").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if((dataSnapshot.getValue().toString()).equals("0"))
                    {
                        node = "";
                        intent = new Intent(MainActivity.this, process.class);
                        simpleProgressBar.setVisibility(View.INVISIBLE);
                        // ENABLE CÁC NÚT NHẤN KHI ĐÃ ĐĂNG NHẬP
                        account_main.setEnabled(true);
                        password_main.setEnabled(true);
                        signup_main.setEnabled(true);
                        signin_main.setEnabled(true);
                        forget_main.setEnabled(true);
                        startActivity(intent);

                    }
                    if((dataSnapshot.getValue().toString()).equals("1"))
                    {
                        node = "";
                        intent = new Intent(MainActivity.this, admin.class);
                        simpleProgressBar.setVisibility(View.INVISIBLE);
                        // ENABLE CÁC NÚT NHẤN KHI ĐÃ ĐĂNG NHẬP
                        account_main.setEnabled(true);
                        password_main.setEnabled(true);
                        signup_main.setEnabled(true);
                        signin_main.setEnabled(true);
                        forget_main.setEnabled(true);
                        startActivity(intent);

                    }
                    if((dataSnapshot.getValue().toString()).equals("2"))
                    {
                        node = "";
                        intent = new Intent(MainActivity.this, doctor.class);
                        intent.putExtra("permit","0");
                        intent.putExtra("email","mail");
                        intent.putExtra("phong","1");
                        simpleProgressBar.setVisibility(View.INVISIBLE);
                        // ENABLE CÁC NÚT NHẤN KHI ĐÃ ĐĂNG NHẬP
                        account_main.setEnabled(true);
                        password_main.setEnabled(true);
                        signup_main.setEnabled(true);
                        signin_main.setEnabled(true);
                        forget_main.setEnabled(true);
                        startActivity(intent);

                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

            overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);



        }
        final Animation blink = AnimationUtils.loadAnimation(this,R.anim.blink);
        final IntentIntegrator i = new IntentIntegrator(this);

        onStart();
        signin_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signin_main.startAnimation(blink);
                if((account_main.getText().toString()).equals("") || (password_main.getText().toString()).equals(""))
                {
                    Toast.makeText(MainActivity.this, "Fill in your Account and Password",Toast.LENGTH_LONG).show();
                }
                else
                {
                    dangnhap();
                }
            }
        });
        signup_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup_main.startAnimation(blink);
                intent = new Intent(MainActivity.this, signup.class);
                intent.putExtra("quyen","0");
                startActivity(intent);
                overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
            }
        });
        forget_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forget_main.startAnimation(blink);
                reset_password();
                dialog.show();
                reset_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        reset_ok.startAnimation(blink);
                        if(reset_mail.getText().toString().equals("")||reset_id.getText().toString().equals(""))
                        {
                            Toast.makeText(MainActivity.this, "Fill in Email and Identification, please", Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            vitri_batdau = (reset_mail.getText().toString()).indexOf("@");
                            node = node + reset_mail.getText().toString().substring(0,vitri_batdau);
                            vitri_ketthuc = (reset_mail.getText().toString()).indexOf(".com");
                            node = node + reset_mail.getText().toString().substring(vitri_batdau + 1,vitri_ketthuc);
                            Log.d("hunahuna", "node : " + node);
                            mData.child("USERS").child(node).addChildEventListener(new ChildEventListener() {
                                @Override
                                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                                    mAuth.sendPasswordResetEmail(reset_mail.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if(task.isSuccessful())
                                            {
                                                Toast.makeText(MainActivity.this, "Check your Email to change Password",Toast.LENGTH_LONG).show();
                                                dialog.cancel();
                                            }
                                            else
                                            {
                                                Toast.makeText(MainActivity.this, "Wrong Email or Id",Toast.LENGTH_LONG).show();
                                            }
                                            node = "";
                                        }
                                    });
                                }

                                @Override
                                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                                }

                                @Override
                                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                                }

                                @Override
                                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                        }
                    }
                });
            }
        });
    }
    private void updateUI(FirebaseUser user) {
        if (user != null)
        {
            if (!user.isEmailVerified())
            {
                // do what you want with UI
            }
        }
    }
    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    void dangnhap()
    {
        String email_si = account_main.getText().toString();
        String pw = password_main.getText().toString();
        mAuth.signInWithEmailAndPassword(email_si, pw)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful())
                        {
                            final ProgressBar simpleProgressBar = (ProgressBar) findViewById(R.id.simpleProgressBar);
                            simpleProgressBar.setVisibility(View.VISIBLE);
                            Log.d("XU LY", "PS2" );
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                            Toast.makeText(MainActivity.this, "Log In Successfully",Toast.LENGTH_LONG).show();
                            vitri_batdau = (account_main.getText().toString()).indexOf("@");
                            node = node + account_main.getText().toString().substring(0,vitri_batdau);
                            vitri_ketthuc = (account_main.getText().toString()).indexOf(".com");
                            node = node + account_main.getText().toString().substring(vitri_batdau + 1,vitri_ketthuc);
                            mData.child("USERS").child(node).child("quyen").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if((dataSnapshot.getValue().toString()).equals("0"))
                                    {
                                        node = "";
                                        intent = new Intent(MainActivity.this, process.class);
                                        simpleProgressBar.setVisibility(View.INVISIBLE);
                                        startActivity(intent);

                                    }
                                    if((dataSnapshot.getValue().toString()).equals("1"))
                                    {
                                        node = "";
                                        intent = new Intent(MainActivity.this, admin.class);
                                        simpleProgressBar.setVisibility(View.INVISIBLE);
                                        startActivity(intent);
                                    }
                                    if((dataSnapshot.getValue().toString()).equals("2"))
                                    {
                                        node = "";
                                        intent = new Intent(MainActivity.this, doctor.class);
                                        intent.putExtra("permit","0");
                                        intent.putExtra("email","mail");
                                        intent.putExtra("phong","1");
                                        simpleProgressBar.setVisibility(View.INVISIBLE);
                                        startActivity(intent);
                                    }
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });

                            overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
                        }
                        else
                        {
                            Toast.makeText(MainActivity.this,"Try Again",Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }
    void reset_password()
    {
        dialog = new Dialog(MainActivity.this);
        dialog.setTitle("Reset Password");
        dialog.setContentView(R.layout.reset_pw);
        reset_mail = (EditText) dialog.findViewById(R.id.mail_reset);
        reset_id = (EditText) dialog.findViewById(R.id.id_reset);
        reset_ok = (Button) dialog.findViewById(R.id.ok_reset);
    }
    void anhxa()
    {
        account_main    = (EditText) findViewById(R.id.account);
        password_main   = (EditText) findViewById(R.id.password);
        signin_main     = (ImageView) findViewById(R.id.signin);
        signup_main     = (ImageView) findViewById(R.id.signup);
        forget_main     = (ImageView) findViewById(R.id.forget);
    }
}
