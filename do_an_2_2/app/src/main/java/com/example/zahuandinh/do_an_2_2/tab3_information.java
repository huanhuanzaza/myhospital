package com.example.zahuandinh.do_an_2_2;


import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;


/**
 * A simple {@link Fragment} subclass.
 */
public class tab3_information extends Fragment {
View view;
ImageView avatar_tab3, qr_tab3, check_tab3, history_tab3, medicalfile_tab3, setting_tab3, logout_tab3, show_qr_tab3, tilte_qr;
TextView mail_tab3, gender_tab3, dob_tab3, verify_tab3, name_tab3;
DatabaseReference mData;
FirebaseStorage storage = FirebaseStorage.getInstance();
StorageReference storageRef = storage.getReferenceFromUrl("gs://myhospital-4f3a6.appspot.com/");
Button ok_tab3;
FirebaseUser user;
Dialog dialog;
FirebaseAuth mAuth;
int vitri_batdau, vitri_ketthuc;
Boolean emailVerified = false;
String email, node = "";
    public tab3_information() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_tab3_information, container, false);
        anhxa();
        final Animation blink = AnimationUtils.loadAnimation(getContext(),R.anim.blink);
        mAuth = FirebaseAuth.getInstance();
        user = FirebaseAuth.getInstance().getCurrentUser();
        mData = FirebaseDatabase.getInstance().getReference();
        email = user.getEmail();
        // TÌM TÊN CHILD BẰNG CÁCH CẮT CHUỖI CỦA EMAIL
        vitri_batdau = email.indexOf("@");
        node = node + email.substring(0,vitri_batdau);
        vitri_ketthuc = email.indexOf(".com");
        node = node + email.substring(vitri_batdau + 1,vitri_ketthuc);
        Log.d("HUHUHU", "meil: " + node);
        mData.child("USERS").child(node).child("name_signup").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                // CẬP NHẬT THÔNG TIN NGƯỜI DÙNG
                UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                        .setDisplayName(dataSnapshot.getValue().toString())

                        //.setPhotoUri(Uri.parse("https://www.facebook.com/photo.php?fbid=1259286560877946&set=a.110128472460433&type=3&theater"))
                        .setPhotoUri(Uri.parse("E://DO_AN_DH//DO_AN_TN//Hinh_anh//huan.JPG"))

                        .build();
                user.updateProfile(profileUpdates)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Log.d("HUHUHU", "User profile updated.");
                                }
                            }
                        });
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        //LẤY THÔNG TIN NGÀY SINH
        mData.child("USERS").child(node).child("dob_signup").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                dob_tab3.setText(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        mData.child("USERS").child(node).child("gender_signup").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                gender_tab3.setText(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        //ĐỌC THÔNG TIN CỦA NGƯỜI DÙNG
        if (user != null) {
            // Name, email address, and profile photo Url
            String name = user.getDisplayName();
            name_tab3.setText(name);

            Uri photoUrl = user.getPhotoUrl();
            Log.d("HUNAZA","NAME = " + photoUrl);
            mail_tab3.setText(email);
            // Check if user's email is verified
            emailVerified = user.isEmailVerified();
            if(emailVerified == false)
            {
                verify_tab3.setText("NOT VERIFIED");
                check_tab3.setImageResource(R.drawable.close);
            }
            else
            {
                verify_tab3.setText("VERIFIED");
                check_tab3.setImageResource(R.drawable.right);
            }
            Log.d("zahuanhuan","check: " + emailVerified);
            String uid = user.getUid();
        }
        emailVerified = user.isEmailVerified();
        if(emailVerified == false)
        {
            check_tab3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FirebaseAuth auth = FirebaseAuth.getInstance();
                    final FirebaseUser user = auth.getCurrentUser();

                    user.sendEmailVerification()
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        Log.d("huanhuan", "Email sent.");
                                        Toast.makeText(getContext(), "Email Verification is Sent", Toast.LENGTH_LONG).show();
                                    }
                                    else {
                                        Toast.makeText(getContext(), "Fail to sent Verified Email", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                }
            });
        }
        qr_tab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show_qr();
                dialog.show();
                show_qr_tab3 = (ImageView) dialog.findViewById(R.id.im_qr);

                //SỬ DỤNG CÂU LỆNH CỦA FIREBASE STORAGE ĐỂ LẤY LINK DOWN
                storageRef.child(node + ".png").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        //SỬ DỤNG THƯ VIỆN PICASSO ĐỂ LOAD HÌNH LÊN IMAGE VIEW
                        Picasso.get()
                                .load(uri)
                                .resize(200, 200)
                                .centerCrop()
                                .into(show_qr_tab3);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle any errors
                    }
                });
            }
        });
        medicalfile_tab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                medicalfile_tab3.startAnimation(blink);
                Intent intent = new Intent(getContext(),medical_file.class);
                intent.putExtra("mode","1");
                startActivity(intent);
            }
        });
        setting_tab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setting_tab3.startAnimation(blink);
                Intent intent = new Intent(getContext(),setting_infor.class);
                intent.putExtra("node", node);
                startActivity(intent);
            }
        });
        history_tab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                history_tab3.startAnimation(blink);
                Intent intent = new Intent(getContext(),history.class);
                intent.putExtra("node", node);
                startActivity(intent);
            }
        });
        logout_tab3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout_tab3.startAnimation(blink);
                Toast.makeText(getContext(),"Sign out Successfully", Toast.LENGTH_LONG).show();
                FirebaseAuth.getInstance().signOut();
                getActivity().finish();
            }
        });
        return view;
    }
    void show_qr()
    {
        dialog = new Dialog(getContext());
        dialog.setTitle("QR CODE");
        dialog.setContentView(R.layout.show_qrcode);
        tilte_qr = (ImageView) dialog.findViewById(R.id.title_qr);
        tilte_qr.setVisibility(View.INVISIBLE);
        ok_tab3 = (Button) dialog.findViewById(R.id.save_qr);
        ok_tab3.setVisibility(View.INVISIBLE);
    }
    void anhxa()
    {
        avatar_tab3 = (ImageView) view.findViewById(R.id.imageView);
        qr_tab3     = (ImageView) view.findViewById(R.id.qr_nho);
        check_tab3 = (ImageView) view.findViewById(R.id.stt);
        history_tab3 = (ImageView) view.findViewById(R.id.history);
        medicalfile_tab3 = (ImageView) view.findViewById(R.id.medicalfile);
        setting_tab3 = (ImageView) view.findViewById(R.id.setting);
        logout_tab3 = (ImageView) view.findViewById(R.id.logout);
        mail_tab3 = (TextView) view.findViewById(R.id.ten);
        gender_tab3 = (TextView) view.findViewById(R.id.sex);
        dob_tab3 = (TextView) view.findViewById(R.id.verify);
        verify_tab3 = (TextView) view.findViewById(R.id.emailcheck);
        name_tab3 = (TextView) view.findViewById(R.id.name);
    }
}
