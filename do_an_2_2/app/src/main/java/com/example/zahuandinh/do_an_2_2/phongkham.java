package com.example.zahuandinh.do_an_2_2;

import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class phongkham extends AppCompatActivity {
ImageView but_add;
TextView total_room_txt, add_mail_room;
Button but_return, but_hospital, add_ok, but_yes, but_no;
ListView listView;
room_shownumber[] room = new room_shownumber[25];
int soluong = 0, total_number, check_number, vitri_ketthuc, vitri_batdau, vitri, tt = 0,stt = 0,a = 0;
String node="", doctor, sophong;
DatabaseReference mData = FirebaseDatabase.getInstance().getReference();;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phongkham);
        anhxa();
        final List<room_shownumber> image_details = new ArrayList<room_shownumber>();
        final Animation blink = AnimationUtils.loadAnimation(this,R.anim.blink);
        final CustomListAdapter adapter = new CustomListAdapter(phongkham.this, image_details);
        mData.child("ROOM").child("soluong").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                soluong = ((Long)dataSnapshot.getValue()).intValue();
                total_room_txt.setText("Total: " + String.valueOf(soluong));
                if(a == 0)
                {
                    a = 1;
                }
                else {
                    stt = 1;
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        mData.child("ROOM").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                node = dataSnapshot.getValue().toString();
                if(node.length() > 5)
                {
                    tt++;
                    //cắt số phòng
                    vitri_ketthuc = node.indexOf(", t");
                    sophong =node.substring(9,vitri_ketthuc);
                    //cắt total number
                    vitri_batdau = node.indexOf("total_number=");
                    vitri_ketthuc = node.indexOf(", check_number");
                    total_number =Integer.valueOf(node.substring(vitri_batdau+13,vitri_ketthuc));
                    //cắt check number
                    vitri_batdau = node.indexOf("check_number=");
                    vitri_ketthuc = node.indexOf(", doctor");
                    check_number =Integer.valueOf(node.substring(vitri_batdau+13,vitri_ketthuc));
                    //cắt tên doctor
                    vitri_batdau = node.indexOf("doctor=");
                    vitri_ketthuc = node.indexOf("}");
                    doctor = node.substring(vitri_batdau+7,vitri_ketthuc);
                    room[tt] = new room_shownumber(doctor,sophong,check_number,total_number);
                    image_details.add(room[tt]);
                    listView.setAdapter(adapter);
                    if(tt == soluong)
                    {
                        tt = 0;
                        Log.d("dinhzahuan", "tt: " + tt);
                        stt = 0;
                    }
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                node = dataSnapshot.getValue().toString();
                if (stt == 0) {
                    if (node.length() > 5) {
                        //cắt số phòng
                        vitri_ketthuc = node.indexOf(", t");
                        sophong = node.substring(9, vitri_ketthuc);
                        //cắt total number
                        vitri_batdau = node.indexOf("total_number=");
                        vitri_ketthuc = node.indexOf(", check_number");
                        total_number = Integer.valueOf(node.substring(vitri_batdau + 13, vitri_ketthuc));
                        //cắt check number
                        vitri_batdau = node.indexOf("check_number=");
                        vitri_ketthuc = node.indexOf(", doctor");
                        check_number = Integer.valueOf(node.substring(vitri_batdau + 13, vitri_ketthuc));
                        //cắt tên doctor
                        vitri_batdau = node.indexOf("doctor=");
                        vitri_ketthuc = node.indexOf("}");
                        doctor = node.substring(vitri_batdau + 7, vitri_ketthuc);
                        vitri = Integer.valueOf(sophong.substring(2));
                        Log.d("dinhzahuan", "vitri**: " + vitri);
                        room[vitri] = new room_shownumber(doctor, sophong, check_number, total_number);
                        image_details.set(vitri - 1, room[vitri]);
                        adapter.notifyDataSetChanged();
                        Log.d("dinhzahuan", "total: " + doctor);
                    }
                }
                if(stt == 1)
                {
                    if(node.length() > 5)
                    {
                        tt++;
                        //cắt số phòng
                        vitri_ketthuc = node.indexOf(", t");
                        sophong =node.substring(9,vitri_ketthuc);
                        //cắt total number
                        vitri_batdau = node.indexOf("total_number=");
                        vitri_ketthuc = node.indexOf(", check_number");
                        total_number =Integer.valueOf(node.substring(vitri_batdau+13,vitri_ketthuc));
                        //cắt check number
                        vitri_batdau = node.indexOf("check_number=");
                        vitri_ketthuc = node.indexOf(", doctor");
                        check_number =Integer.valueOf(node.substring(vitri_batdau+13,vitri_ketthuc));
                        //cắt tên doctor
                        vitri_batdau = node.indexOf("doctor=");
                        vitri_ketthuc = node.indexOf("}");
                        doctor = node.substring(vitri_batdau+7,vitri_ketthuc);
                        room[tt] = new room_shownumber(doctor,sophong,check_number,total_number);
                        image_details.add(room[tt]);
                        listView.setAdapter(adapter);
                        if(tt == soluong)
                        {
                            tt = 0;
                            Log.d("dinhzahuan", "tt: " + tt);
                            stt = 0;
                        }
                    }
                }
            }
            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        but_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog;
                dialog = new Dialog(phongkham.this);
                dialog.setTitle("Add Room");
                dialog.setContentView(R.layout.addroom);
                add_mail_room = (EditText) dialog.findViewById(R.id.add_txt_room);
                add_ok = (Button) dialog.findViewById(R.id.add_ok);
                dialog.show();
                add_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("dinhzahuan", "soluong + 1 : " + soluong);
                        add_ok.startAnimation(blink);
                        room[soluong + 1] = new room_shownumber(add_mail_room.getText().toString(),"so" + String.valueOf(soluong+1),0,0);
                        mData.child("ROOM").child("soluong").setValue(soluong + 1);
                        mData.child("ROOM").child(String.valueOf(soluong + 1)).setValue(room[soluong + 1]);
                        if(stt == 0)
                            a = 0;
                        dialog.cancel();
                    }
                });
            }
        });
        but_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(phongkham.this,admin.class );
                startActivity(intent);
                overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
                finish();
            }
        });
        but_hospital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(phongkham.this,process.class );
                startActivity(intent);
                overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
                finish();
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                final Dialog dialog;
                dialog = new Dialog(phongkham.this);
                dialog.setTitle("Delete Room");
                dialog.setContentView(R.layout.yesno);
                but_yes = (Button) dialog.findViewById(R.id.yes);
                but_no = (Button) dialog.findViewById(R.id.no);
                dialog.show();
                but_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        but_yes.startAnimation(blink);
                        mData.child("ROOM").child("soluong").setValue(soluong -1 );
                        mData.child("ROOM").child(String.valueOf(position + 1)).removeValue();
                        Log.d("dinhzahuan","position: " + position);
                        Intent intent = new Intent(phongkham.this,phongkham.class );
                        startActivity(intent);
                        dialog.cancel();
                    }
                });
                but_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
                return false;
            }
        });
    }
    void anhxa()
    {
        listView = (ListView) findViewById(R.id.add_lv);
        but_add = (ImageView) findViewById(R.id.add);
        total_room_txt = (TextView) findViewById(R.id.txt_total);
        but_return = (Button) findViewById(R.id.but_return);
        but_hospital = (Button) findViewById(R.id.but_hospital);
    }
}
