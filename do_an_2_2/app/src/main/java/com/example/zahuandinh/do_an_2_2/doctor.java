package com.example.zahuandinh.do_an_2_2;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

public class doctor extends AppCompatActivity {
TextView txt_doc, txt_dialog, txt_now, txt_total;
FirebaseUser user;
Intent intent;
Dialog dialog;
DatabaseReference mData = FirebaseDatabase.getInstance().getReference();
ImageView dieuhuong_doc, qr_doc, write_doc, file_doc, name_doc;
Button next_doc, absent_doc, btn_yes, btn_no;
String phong="", email="", doctor_name="",node="", node_mail="", permit="", dr_name, date_time="", now_total_room = "0";
int scan, vitri_batdau, vitri_ketthuc, thutu,total_number, phat, yy, dd,mmm, minute, hour, denta,n, w_checknum;
double wait;
Calendar calendar = Calendar.getInstance();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor);
        anhxa();
        date_update update = new date_update();
        update.update();
        Log.d("hahahaha","phong: " + phong);
        //LẤY THÔNG TIN VỀ CHO PHÉP
        intent = getIntent();
        permit = intent.getStringExtra("permit");
        email = intent.getStringExtra("email");
        Log.d("hhhhhh","permit : " + permit);
        phong = intent.getStringExtra("phong");
        //Lấy n
        mData.child("ROOM").child(phong).child("n").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists() == true)
                {
                    n = ((Long) dataSnapshot.getValue()).intValue();
                    Log.d("doctor", "n: " + n);

                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        //Lấy thông tin w_checknum
        mData.child("ROOM").child(phong).child("w_checknum").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists() == true)
                {
                    w_checknum = ((Long) dataSnapshot.getValue()).intValue();
                    Log.d("doctor", "w_checknum: " + w_checknum);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        //Lấy thông tin mốc thời gian hour
        mData.child("ROOM").child(phong).child("w_hour").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists() == true)
                {
                    hour = ((Long) dataSnapshot.getValue()).intValue();
                    Log.d("gio_kham", "get hour: " + hour);

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        mData.child("ROOM").child(phong).child("w_minute").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists() == true)
                {
                    minute = ((Long) dataSnapshot.getValue()).intValue();
                    Log.d("gio_kham", "get minute: " + minute);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        //Lấy thông tin của thời gian chờ đơn vị
        mData.child("ROOM").child(phong).child("w_wait").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                wait = (double) dataSnapshot.getValue();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        //Lấy thông tin về số thứ tự khám hiện tại
        mData.child("ROOM").child(phong).child("check_number").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                thutu = ((Long) dataSnapshot.getValue()).intValue();
                if(permit.equals("0")  && now_total_room.equals("0"))
                {
                    txt_now.setText("NOW: 0");
                }
                else
                {
                    txt_now.setText("NOW: " + thutu);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        //lấy giá trị tổng số bệnh nhân của phòng khám để so sánh.
        mData.child("ROOM").child(phong).child("total_number").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                total_number = ((Long) dataSnapshot.getValue()).intValue();
                if(permit.equals("0") && now_total_room.equals("0"))
                {
                    txt_total.setText("TOTAL: 0");
                }
                else
                {
                    txt_total.setText("TOTAL: " + total_number);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        Log.d("hahahaha","permit" + permit);
        final Animation blink = AnimationUtils.loadAnimation(this,R.anim.blink);
        final IntentIntegrator i = new IntentIntegrator(this);
        user = FirebaseAuth.getInstance().getCurrentUser();
        //tìm child của doctor
        doctor_name = user.getEmail();
        vitri_batdau = doctor_name.indexOf("@");
        node = node + doctor_name.substring(0,vitri_batdau);
        vitri_ketthuc = doctor_name.indexOf(".com");
        node = node + doctor_name.substring(vitri_batdau + 1,vitri_ketthuc);

        // lấy thông tin tên bác sĩ
        mData.child("USERS").child(node).child("name_signup").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(permit.equals("0")  && now_total_room.equals("0"))
                {
                    txt_doc.setText(dataSnapshot.getValue().toString() + "\n" + "Room: Check in, please");
                }
                else
                {
                    txt_doc.setText(dataSnapshot.getValue().toString() + "\n" + "Room: " + phong);
                }
                dr_name = dataSnapshot.getValue().toString();
                Log.d("hahahaha","doctor's name: " + dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        if(permit.equals("0"))
        {
            absent_doc.setEnabled(false);
            next_doc.setEnabled(false);
            qr_doc.setEnabled(false);
            file_doc.setEnabled(false);
            write_doc.setEnabled(false);
            dieuhuong_doc.setEnabled(false);
        }
        if(permit.equals("1"))
        {
            absent_doc.setEnabled(true);
            next_doc.setEnabled(true);
            qr_doc.setEnabled(true);
            file_doc.setEnabled(true);
            write_doc.setEnabled(true);
            dieuhuong_doc.setEnabled(true);
        }
        name_doc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name_doc.startAnimation(blink);
                scan = 0;
                i.initiateScan();
            }
        });
        qr_doc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qr_doc.startAnimation(blink);
                scan = 1;
                i.initiateScan();
            }
        });
        next_doc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next_doc.startAnimation(blink);
                //Lấy thời gian hiện tại
                if(thutu >= total_number)
                {
                    Toast.makeText(doctor.this, "No Patients !!!",Toast.LENGTH_LONG).show();
                }
                else
                {
                    //Nếu mới bắt đầu khám thì chỉ cập nhật giờ bắt đầu gọi bệnh nhân
                    if(thutu == 1)
                    {
                        //Lấy thời gian hiện tại
                        hour = calendar.get(Calendar.HOUR_OF_DAY);
                        minute = calendar.get(Calendar.MINUTE);
                        mData.child("ROOM").child(phong).child("w_hour").setValue(hour);
                        mData.child("ROOM").child(phong).child("w_minute").setValue(minute);
                        mData.child("ROOM").child(phong).child("flag").setValue(1);

                    }
                    //Ngược lại thì tính thời gian khám cho bệnh nhân đó --> tính toán và cập nhật
                    else
                    {
                        Calendar cal = Calendar.getInstance();

                        //Trường hợp 2 lần gọi liên tiếp trong cùng 1 giờ thì chỉ cần trừ số phút của người sau với người trước
                        if(hour == cal.get(Calendar.HOUR_OF_DAY))
                        {
                            Log.d("gio_kham", "TH1  ****************");
                            denta = cal.get(Calendar.MINUTE);
                            Log.d("du_doan","now: " + denta);
                            denta = denta - minute;
                            minute = cal.get(Calendar.MINUTE);
                            Log.d("du_doan","before: " + minute);
                            Log.d("du_doan","denta 1: " + denta);
                            Log.d("du_doan","thutu 1: " + thutu);
                            Log.d("du_doan","checknum+ 8   1: " + (w_checknum + 8));
                            //THỜI GIAN TIẾP NHẬN BỆNH NHÂN, CHUẨN ĐOÁN BAN ĐẦU CHỈ ĐƯỢC TỐI ĐA 10P ==> NHỮNG TRƯỜNG HỢP QUÁ 10P COI NHƯ NGOẠI LỆ
                            //KHÔNG ĐƯỢC SỬ DỤNG ĐỂ TÍNH THỜI GIAN CHỜ ĐƠN VỊ NÊN SẼ KHÔNG ĐƯỢC CẬP NHẬT LÊN FIREBASE
                            if((denta > 8 || denta < 4) || (thutu == (w_checknum + 8)) )
                            {
                                mData.child("ROOM").child(phong).child("w_hour").setValue(cal.get(Calendar.HOUR_OF_DAY));
                                mData.child("ROOM").child(phong).child("w_minute").setValue(cal.get(Calendar.MINUTE));
                                mData.child("ROOM").child(phong).child("w_checknum").setValue(thutu+1);
                                mData.child("ROOM").child(phong).child("flag").setValue(1);
                                //wait = (wait + denta)/2
                            }
                            else
                            {
                                wait = ((wait*n)+denta)/(n+1);
                                n = n+1;
                                mData.child("ROOM").child(phong).child("n").setValue(n);
                                mData.child("ROOM").child(phong).child("w_wait").setValue(wait);
                            }
                        }
                        //Ngược lại, nếu giờ khám của người sau khác với vs người trước ta cộng 60 cho người sau rồi trừ cho người trước
                        else
                        {

                            Log.d("gio_kham", "TH2  ****************");
                            denta = cal.get(Calendar.MINUTE);
                            Log.d("du_doan","before: " + minute);
                            //Nếu khoảng cách khám khác giờ thì ta tính độ chênh lệch giờ rồi *60 + denta - minute (cũ)
                            denta = ((cal.get(Calendar.HOUR_OF_DAY))-hour)*60 + denta - minute;
                            minute = cal.get(Calendar.MINUTE);
                            Log.d("du_doan","denta 2: " + denta);
                            //THỜI GIAN TIẾP NHẬN BỆNH NHÂN, CHUẨN ĐOÁN BAN ĐẦU CHỈ ĐƯỢC TỐI ĐA 10P ==> NHỮNG TRƯỜNG HỢP QUÁ 10P COI NHƯ NGOẠI LỆ
                            //KHÔNG ĐƯỢC SỬ DỤNG ĐỂ TÍNH THỜI GIAN CHỜ ĐƠN VỊ NÊN SẼ KHÔNG ĐƯỢC CẬP NHẬT LÊN FIREBASE
                            if((denta > 8 || denta < 4) || (thutu == (w_checknum + 8)) )
                            {
                                //wait = (wait + denta)/2;
                                mData.child("ROOM").child(phong).child("w_hour").setValue(cal.get(Calendar.HOUR_OF_DAY));
                                mData.child("ROOM").child(phong).child("w_minute").setValue(cal.get(Calendar.MINUTE));
                                mData.child("ROOM").child(phong).child("w_checknum").setValue(thutu+1);
                                mData.child("ROOM").child(phong).child("flag").setValue(1);
                            }
                            else
                            {
                                wait = ((wait*n)+denta)/(n+1);
                                mData.child("ROOM").child(phong).child("w_wait").setValue(wait);
                                n ++;
                                mData.child("ROOM").child(phong).child("n").setValue(n);
                            }
                        }
                        //Lấy mốc thời gian gọi bệnh nhân kế tiếp
//                        hour = cal.get(Calendar.HOUR_OF_DAY);
//                        minute = cal.get(Calendar.MINUTE);
//                        mData.child("ROOM").child(phong).child("w_hour").setValue(hour);
//                        mData.child("ROOM").child(phong).child("w_minute").setValue(minute);
//                        Log.d("gio_kham", "hour... = " + hour  + "  minute... = " + minute);
//                        Log.d("gio_kham", "denta = " + denta + "\n" + "wait = " + wait);
                    }
                    thutu++;
                    mData.child("ROOM").child(phong).child("check_number").setValue(thutu);
                    //Refesh activity
                    /*finish();
                    startActivity(getIntent());*/
                }
                write_doc.setEnabled(false);
                file_doc.setEnabled(false);
                dieuhuong_doc.setEnabled(false);
                node_mail = "";
                permit = "0";
                now_total_room = "1";
            }
        });
        //Nhấn nút báo bệnh nhân VẮNG --> PHẠT
        absent_doc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                absent_doc.startAnimation(blink);
                //final Dialog dialog;
                dialog = new Dialog(doctor.this);
                dialog.setTitle("Add Room");
                dialog.setContentView(R.layout.yesno);
                txt_dialog = (TextView) dialog.findViewById(R.id.topic);
                txt_dialog.setText("This patient is absent ?");
                btn_yes = (Button) dialog.findViewById(R.id.yes);
                btn_no = (Button) dialog.findViewById(R.id.no);
                dialog.show();
                now_total_room = "1";
                //XÁC NHẬN VẮNG
                btn_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Lấy thông tin ngày tháng năm

                        int year = calendar.get(calendar.YEAR);
                        yy = year;
                        int month = calendar.get(calendar.MONTH);
                        mmm = month + 1;
                        final int day = calendar.get(calendar.DAY_OF_MONTH);
                        dd = day;
                        date_time = String.valueOf(dd) + "-" + String.valueOf(mmm) + "-" + String.valueOf(yy);
                        // Tải danh sách lịch khám và kiểm tra
                        mData.child("SCHEDULE").child(date_time).child(phong).child(String.valueOf(thutu)).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@Nullable DataSnapshot dataSnapshot) {
                               // Log.d("phat_tien", dataSnapshot.getValue().toString());

                                if(dataSnapshot.exists() == false)
                                {

                                }
                                else
                                {
                                    final String mail= dataSnapshot.getValue().toString();
                                    Log.d("phat_tien", "mail: " + mail);
                                    mData.child("USERS").child(dataSnapshot.getValue().toString()).child("phat").addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                            phat = Integer.valueOf( dataSnapshot.getValue().toString());
                                            Log.d("phat_tien", "phat = " + String.valueOf(phat));
                                            if(phat == 3)
                                            {
                                                Toast.makeText(doctor.this, "This Patient was locked", Toast.LENGTH_LONG).show();
                                            }
                                            else
                                            {
                                                phat++;
                                                mData.child("USERS").child(mail).child("phat").setValue(phat);
                                            }
                                        }

                                        @Override
                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                        }
                                    });

                                }

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                        //KIỂM TRA SỐ LƯỢNG --> TĂNG SỐ THỨ TỰ VÀ CẬP NHẬT
                        if(thutu >= total_number)
                        {
                            Toast.makeText(doctor.this, "No Patients !!!",Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            thutu++;
                            mData.child("ROOM").child(phong).child("check_number").setValue(thutu);
                        }
                        //mData.child("USERS").child(email).child("phat").setValue(phat);
                        write_doc.setEnabled(false);
                        file_doc.setEnabled(false);
                        dieuhuong_doc.setEnabled(false);
                        node_mail = "";
                        permit = "0";
                        dialog.cancel();
                    }
                });
                btn_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
            }
        });
        write_doc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                write_doc.startAnimation(blink);
                if(permit.equals("0"))
                {
                    vitri_batdau = email.indexOf("@");
                    node_mail = node_mail + email.substring(0,vitri_batdau);
                    vitri_ketthuc = email.indexOf(".com");
                    node_mail = node_mail + email.substring(vitri_batdau + 1,vitri_ketthuc);
                }
                if(permit.equals("1"))
                    node_mail = email;
                Intent intent = new Intent(doctor.this,write_medical.class);
                intent.putExtra("email",node_mail);
                intent.putExtra("phong",phong);
                startActivity(intent);
                overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
                finish();
            }
        });
        file_doc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                file_doc.startAnimation(blink);
                if(permit.equals("0"))
                {
                    vitri_batdau = email.indexOf("@");
                    node_mail = node_mail + email.substring(0,vitri_batdau);
                    vitri_ketthuc = email.indexOf(".com");
                    node_mail = node_mail + email.substring(vitri_batdau + 1,vitri_ketthuc);
                }
                if(permit.equals("1"))
                    node_mail = email;
                Intent intent = new Intent(doctor.this,medical_file.class);
                intent.putExtra("email",node_mail);
                intent.putExtra("phong",phong);
                intent.putExtra("mode","0");
                startActivity(intent);
                overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
                //finish();
            }
        });
        dieuhuong_doc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dieuhuong_doc.startAnimation(blink);
                if(permit.equals("0"))
                {
                    vitri_batdau = email.indexOf("@");
                    node_mail = node_mail + email.substring(0,vitri_batdau);
                    vitri_ketthuc = email.indexOf(".com");
                    node_mail = node_mail + email.substring(vitri_batdau + 1,vitri_ketthuc);
                }
                if(permit.equals("1"))
                    node_mail = email;
                Intent intent = new Intent(doctor.this,list_specialist.class);
                intent.putExtra("email",node_mail);
                intent.putExtra("phong",phong);
                intent.putExtra("dieuhuong",1);
                startActivity(intent);
                overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null)
        {
            if(result.getContents() == null) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            }
            else
            {
                //Picasso.get().load(result.getContents()).into(qr_i);
                try {
                    JSONObject jsonObject = new JSONObject(result.getContents());
                    // scan = 0 --> quét QR lần đầu để check in phòng
                    if(scan == 0)
                    {
                        phong = jsonObject.getString("room");
                        now_total_room = "1";
                        Log.d("hahahaha", "phong: " + phong);
                        if(phong.length() < 2)
                        {
                            //TĂNG SỐ THỨ TỰ VÀO KHÁM LÊN 1 KHI BS VÀO CHECK IN
                            mData.child("ROOM").child(phong).child("check_number").addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    thutu = ((Long) dataSnapshot.getValue()).intValue();
                                    if(thutu == 0)
                                    {
                                        thutu = 1;
                                        mData.child("ROOM").child(phong).child("check_number").setValue(thutu);
                                    }
                                    txt_now.setText("NOW: " + String.valueOf(thutu));
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                            //ĐỌC GIÁ TRỊ TỔNG BỆNH NHÂN ĐÃ LẤY SỐ CHO PHÒNG ĐÓ
                            mData.child("ROOM").child(phong).child("total_number").addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    total_number = ((Long) dataSnapshot.getValue()).intValue();
                                    txt_total.setText("TOTAL: " + String.valueOf(total_number));
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                            //cập nhật tên bác sĩ cho phòng khám
                            mData.child("ROOM").child(phong).child("doctor").setValue(dr_name);
                            qr_doc.setEnabled(true);
                            next_doc.setEnabled(true);
                            absent_doc.setEnabled(true);

                            Toast.makeText(doctor.this, "Scan Room " + phong + " Successfully", Toast.LENGTH_LONG).show();
                            //Hiển thị tên Bác sĩ và phòng đang khám
                            txt_doc.setText(dr_name + "\n" + "Room: " + phong);
                            if(hour == 6 && minute == 0)
                            {
                                //Lấy mốc thời gian khám của phòng
                                hour = calendar.get(Calendar.HOUR_OF_DAY);
                                minute = calendar.get(Calendar.MINUTE);
                                mData.child("ROOM").child(phong).child("w_hour").setValue(hour);
                                mData.child("ROOM").child(phong).child("w_minute").setValue(minute);
                                mData.child("ROOM").child(phong).child("flag").setValue(1);
                            }
                            mData.child("ROOM").child(phong).child("w_hour").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if(dataSnapshot.exists() == true)
                                    {
                                        hour = ((Long) dataSnapshot.getValue()).intValue();
                                        Log.d("gio_kham", "get hour khi cập nhật: " + hour);

                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });

                            mData.child("ROOM").child(phong).child("w_minute").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if(dataSnapshot.exists() == true)
                                    {
                                        minute = ((Long) dataSnapshot.getValue()).intValue();
                                        Log.d("gio_kham", "get minute  khi cập nhật: " + minute);
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                        }
                        // Nếu tên phòng thuộc dạng phòng chuyên khoa thì chuyển sang phòng chuyên khoa
                        else
                        {
                            Intent intent = new Intent(doctor.this,specialist.class);
                            intent.putExtra("dr_name", dr_name);
                            Log.d("ehuanoi", "dr: " + dr_name);
                            intent.putExtra("room_name",phong);
                            intent.putExtra("permit","0");
                            startActivity(intent);
                            overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
                            finish();
                        }
                    }
                    // scan = 1 --> quét QR để quét tên bệnh nhân
                    if(scan == 1)
                    {
                        email = jsonObject.getString("name");
                        Log.d("hahahaha", "email: " + email);
                        write_doc.setEnabled(true);
                        file_doc.setEnabled(true);
                        dieuhuong_doc.setEnabled(true);
                        Toast.makeText(doctor.this, "Scan Patient " + email + " Successfully", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        else
        {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
    void anhxa()
    {
        txt_now = (TextView) findViewById(R.id.dr_now);
        txt_total = (TextView) findViewById(R.id.dr_total);
        txt_doc = (TextView) findViewById(R.id.doc_txt);
        dieuhuong_doc = (ImageView) findViewById(R.id.doc_calendar);
        qr_doc = (ImageView) findViewById(R.id.doc_qr);
        write_doc = (ImageView) findViewById(R.id.doc_write);

        file_doc = (ImageView) findViewById(R.id.doc_file);
        name_doc = (ImageView) findViewById(R.id.doc_name);
        next_doc = (Button) findViewById(R.id.doc_next);
        absent_doc = (Button) findViewById(R.id.doc_absent);
    }
}
