package com.example.zahuandinh.do_an_2_2;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;

public class write_medical extends AppCompatActivity {
EditText trieuchung, chuandoan, chidan;
Button button_ok, button_return;
DatabaseReference mData = FirebaseDatabase.getInstance().getReference();
Intent intent;
TextView patient;
String email, time_check="", phong, dr_name;
Calendar calendar = Calendar.getInstance();
int year, month, day;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_medical);
        anhxa();
        year = calendar.get(calendar.YEAR);
        month = calendar.get(calendar.MONTH);
        month = month +1;
        day = calendar.get(calendar.DAY_OF_MONTH);
        time_check = day + "-" + month + "-" + year;
        Log.d("hihihihi", "time: " + time_check);
        intent = getIntent();
        email = intent.getStringExtra("email");
        phong = intent.getStringExtra("phong");
        dr_name = intent.getStringExtra("dr_name");
        Log.d("hihihihi", "email: " + email);
        mData.child("USERS").child(email).child("name_signup").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                patient.setText(dataSnapshot.getValue().toString());
                Log.d("hihihihi", "name: " + dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        button_return.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(phong.length() < 2)
                {
                    Intent intent = new Intent(write_medical.this, doctor.class);
                    intent.putExtra("permit","1");
                    intent.putExtra("email",email);
                    intent.putExtra("phong",phong);
                    startActivity(intent);
                }
                else
                {
                    Intent intent = new Intent(write_medical.this, specialist.class);
                    intent.putExtra("permit","1");
                    intent.putExtra("email",email);
                    intent.putExtra("phong",phong);
                    startActivity(intent);
                }
                overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
                finish();
            }
        });
        button_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //if(phong.length() < 2)
                //{
                    mData.child("USERS").child(email).child("benh_an").child(time_check).child("Date").setValue(time_check);
                    mData.child("USERS").child(email).child("benh_an").child(time_check).child(phong).child("phong").setValue(phong);
                    mData.child("USERS").child(email).child("benh_an").child(time_check).child(phong).child("trieu_chung").setValue(trieuchung.getText().toString());
                    mData.child("USERS").child(email).child("benh_an").child(time_check).child(phong).child("chuan_doan").setValue(chuandoan.getText().toString());
                    mData.child("USERS").child(email).child("benh_an").child(time_check).child(phong).child("chi_dan").setValue(chidan.getText().toString());
                //}
               /* else
                {
                    mData.child("USERS").child(email).child("benh_an").child(time_check).child("Date").setValue(time_check);
                    mData.child("USERS").child(email).child("benh_an").child(time_check).child("phong").setValue(phong);
                    mData.child("USERS").child(email).child("benh_an").child(time_check).child("trieu_chung").setValue(trieuchung.getText().toString());
                    mData.child("USERS").child(email).child("benh_an").child(time_check).child("chuan_doan").setValue(chuandoan.getText().toString());
                    mData.child("USERS").child(email).child("benh_an").child(time_check).child("chi_dan").setValue(chidan.getText().toString());
                }*/
                Toast.makeText(write_medical.this, "Medical file is saved", Toast.LENGTH_LONG).show();
                if(phong.length() < 2)
                {
                    Intent intent = new Intent(write_medical.this, doctor.class);
                    intent.putExtra("permit","1");
                    intent.putExtra("email",email);
                    intent.putExtra("phong",phong);
                    startActivity(intent);
                }
                else
                {
                    Log.d("hihanhan", "phong : " + phong);
                    Intent intent = new Intent(write_medical.this, specialist.class);
                    intent.putExtra("dr_name",dr_name);
                    intent.putExtra("permit","1");
                    intent.putExtra("email",email);
                    intent.putExtra("room_name",phong);
                    startActivity(intent);
                }
                overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
                finish();
            }
        });
    }
    void anhxa()
    {
        patient = (TextView) findViewById(R.id.pname);
        trieuchung = (EditText) findViewById(R.id.trieuchung);
        chuandoan = (EditText) findViewById(R.id.chuandoan);
        chidan = (EditText) findViewById(R.id.chidan);
        button_ok = (Button) findViewById(R.id.ok_button);
        button_return = (Button) findViewById(R.id.return_button);
    }
}
