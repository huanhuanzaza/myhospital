package com.example.zahuandinh.do_an_2_2;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class PagerAdapter extends FragmentStatePagerAdapter {

    public PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment frag = null;
        switch (position)
        {
            case 0:
                frag = new tab1_shownumber();
                break;
            case 1:
                frag = new tab2_getnumber();
                break;
            case 2:
                frag = new tab3_information();
                break;
        }
        return frag;
    }

    @Override
    public int getCount() {
        return 3;
    }
    public CharSequence getPageTitle(int position)
    {
        String title ="";
        switch (position)
        {
            case 0:
                title = "My Hospital";
                break;
            case 1:
                title = "Get Number";
                break;
            case 2:
                title = "Personal";
                break;
        }
        return  title;
    }
}
