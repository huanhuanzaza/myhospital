package com.example.zahuandinh.do_an_2_2;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.zxing.WriterException;

import java.io.ByteArrayOutputStream;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;

public class signup extends AppCompatActivity {
information infor;
EditText mail_signup, pw_signup, name_signup, dob_signup, id_signup;
RadioButton male_signup, female_signup;
RadioGroup gender_signup;
Button ok_signup, qr_signup, exit_signup, show_qr_ok;
DatabaseReference mData;
FirebaseAuth mAuth;
FirebaseStorage storage = FirebaseStorage.getInstance();
final StorageReference storageRef = storage.getReference();
TextView saveqr;
ImageView show_qr;
Bitmap bitmap;
QRGEncoder qrgEncoder;
final String[] inputValue = new String[1];
int vitri_batdau, vitri_ketthuc;
int sex = 0;
Intent intent;
String infor_sex, quyen = "0";
String node = "", picture;
Dialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        anhxa();
        intent = getIntent();
        // kiem tra gia tri quyen thiet lap cho user (0: benh nhan ---- 1 : admin ---- 2 : bac si)
        quyen = intent.getStringExtra("quyen");
        final Animation blink = AnimationUtils.loadAnimation(this,R.anim.blink);
        mAuth = FirebaseAuth.getInstance();
        mData = FirebaseDatabase.getInstance().getReference();
        qr_signup.setEnabled(false);
        ok_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // xác định giới tính - xem radio button nào được chọn
                ok_signup.startAnimation(blink);
                sex = gender_signup.getCheckedRadioButtonId();
                switch (sex)
                {
                    case R.id.male:
                        infor_sex = "Male";
                        break;
                    case R.id.female:
                        infor_sex = "Female";
                        break;
                    default:
                    {
                        sex = 0;
                    }
                }
                if((mail_signup.getText().toString()).equals("")|| (pw_signup.getText().toString()).equals("")|| (name_signup.getText().toString()).equals("")|| (dob_signup.getText().toString()).equals("")|| sex == 0)
                {
                    Toast.makeText(signup.this, "Fill in your Informations, please !!!", Toast.LENGTH_LONG).show();
                }
                else
                {
                    vitri_batdau = (mail_signup.getText().toString()).indexOf("@");
                    node = node + mail_signup.getText().toString().substring(0,vitri_batdau);
                    vitri_ketthuc = (mail_signup.getText().toString()).indexOf(".com");
                    node = node + mail_signup.getText().toString().substring(vitri_batdau + 1,vitri_ketthuc);
                    picture = node;
                    dangki();
                }
            }
        });
        qr_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qr_signup.startAnimation(blink);
                show_qr();
                inputValue[0] = ("{"+"\""+"name" + "\"" + ":" +"\""+ (mail_signup.getText().toString().trim())+"\""/* + "," + "\"" + "ID" + "\"" + ":" + "\"" + pw_signup.getText().toString().trim() + "\""*/ + "}").trim();
                if (inputValue[0].length() > 0) {
                    WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
                    Display display = manager.getDefaultDisplay();
                    Point point = new Point();
                    display.getSize(point);
                    int width = point.x;
                    int height = point.y;
                    int smallerDimension = width < height ? width : height;
                    smallerDimension = smallerDimension * 3 / 4;

                    qrgEncoder = new QRGEncoder(
                            inputValue[0], null,
                            QRGContents.Type.TEXT,
                            smallerDimension);
                    try {
                        bitmap = qrgEncoder.encodeAsBitmap();
                        show_qr.setImageBitmap(bitmap);
                    } catch (WriterException e) {

                    }
                } else {
                    //edtValue.setError("Required");
                }
                dialog.show();
                show_qr_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        show_qr_ok.startAnimation(blink);
                        StorageReference mountainsRef = storageRef.child(picture + ".png");
                        show_qr.setDrawingCacheEnabled(true);
                        show_qr.buildDrawingCache();
                        Bitmap bitmap = ((BitmapDrawable) show_qr.getDrawable()).getBitmap();
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                        byte[] data = baos.toByteArray();

                        UploadTask uploadTask = mountainsRef.putBytes(data);
                        uploadTask.addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                Toast.makeText(signup.this, " Save QR Failed ", Toast.LENGTH_LONG).show();
                            }
                        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                Toast.makeText(signup.this, "Save Successfully", Toast.LENGTH_LONG).show();
                                dialog.cancel();
                                finish();
                            }
                        });
                    }
                });
            }
        });
        exit_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exit_signup.startAnimation(blink);
                finish();
            }
        });
    }
    void dangki()
    {
        final FirebaseUser user = mAuth.getCurrentUser();
        String email_a = mail_signup.getText().toString();
        String password_a = pw_signup.getText().toString();
        mAuth.createUserWithEmailAndPassword(email_a, password_a)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            infor = new information(mail_signup.getText().toString(),pw_signup.getText().toString(), name_signup.getText().toString(), id_signup.getText().toString(), infor_sex, dob_signup.getText().toString(),quyen,"schedule",0);
                            //mData.child(ident.getText().toString()).setValue(hs, new DatabaseReference.CompletionListener() {

                            mData.child("USERS").child(node).setValue(infor, new DatabaseReference.CompletionListener() {
                                @Override
                                public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {

                                    if (databaseError == null) {
                                        //mData.child("QUANTITY").setValue(soluong);
                                        Toast.makeText(signup.this, "Success ", Toast.LENGTH_LONG).show();
                                        qr_signup.setEnabled(true);
                                        user.sendEmailVerification()
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()) {
                                                            Toast.makeText(signup.this, "Check Your Email to Verify !!! ", Toast.LENGTH_LONG).show();
                                                            node = "";
                                                        }
                                                    }
                                                });
                                    } else {
                                        Toast.makeText(signup.this, "Try Again ", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                        }
                        else
                        {
                            Toast.makeText(signup.this, "Sign Up Failed ", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }
    void show_qr()
    {
        dialog = new Dialog(signup.this);
        dialog.setTitle("QR CODE");
        dialog.setContentView(R.layout.show_qrcode);
        show_qr = (ImageView) dialog.findViewById(R.id.im_qr);
        show_qr_ok = (Button) dialog.findViewById(R.id.save_qr);
    }
    void anhxa()
    {
        mail_signup = (EditText) findViewById(R.id.mail);
        pw_signup = (EditText) findViewById(R.id.pw);
        name_signup = (EditText) findViewById(R.id.name);
        dob_signup = (EditText) findViewById(R.id.dob);
        gender_signup = (RadioGroup)findViewById(R.id.gender);
        male_signup = (RadioButton) findViewById(R.id.male);
        female_signup = (RadioButton) findViewById(R.id.female);
        ok_signup = (Button) findViewById(R.id.ok_signup);
        qr_signup = (Button) findViewById(R.id.qrcode);
        exit_signup = (Button) findViewById(R.id.exit_signup);
        id_signup = (EditText) findViewById(R.id.id);
    }
}
