package com.example.zahuandinh.do_an_2_2;

public class room_shownumber {
    public String doctor, sophong;
    public int check_number;
    public int total_number;

    public room_shownumber() {
    }

    public room_shownumber(String doctor, String sophong, int check_number, int total_number) {
        this.doctor = doctor;
        this.sophong = sophong;
        this.check_number = check_number;
        this.total_number = total_number;
    }
    public String getDoctor() {
        return doctor;}

    public String getSophong() {
        return sophong;
    }

    public int getCheck_number() {
        return check_number;
    }

    public int getTotal_number() {
        return total_number;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public void setSophong(String sophong) {
        this.sophong = sophong;
    }

    public void setCheck_number(int check_number) {
        this.check_number = check_number;
    }

    public void setTotal_number(int total_number) {
        this.total_number = total_number;
    }
    public String toString(){
        return this.doctor + ": " + this.check_number + " - " + this.total_number;
    }
}
