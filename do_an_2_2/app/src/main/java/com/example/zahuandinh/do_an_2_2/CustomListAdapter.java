package com.example.zahuandinh.do_an_2_2;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class CustomListAdapter extends BaseAdapter {
    private List<room_shownumber> listData;
    private LayoutInflater layoutInflater;
    private Context context;
    public CustomListAdapter(Context aContext, List<room_shownumber> listData){
        this.context = aContext;
        this.listData = listData;
        layoutInflater = LayoutInflater.from(aContext);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if(convertView == null){
            convertView = layoutInflater.inflate(R.layout.layout_shownumber, null);
            holder = new ViewHolder();
            holder.dr = (TextView) convertView.findViewById(R.id.dr);
            holder.num_now = (TextView) convertView.findViewById(R.id.num_txt);
            holder.num_total = (TextView) convertView.findViewById(R.id.total_txt);
            holder.dr_name = (TextView) convertView.findViewById(R.id.name_dr);
            holder.num_now_txt = (TextView) convertView.findViewById(R.id.num_now);
            holder.num_total_txt = (TextView) convertView.findViewById(R.id.num_total);
            holder.number_room = (ImageView) convertView.findViewById(R.id.number);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        room_shownumber room = this.listData.get(position);
        holder.dr_name.setText(room.getDoctor());
        holder.num_now_txt.setText(String.valueOf(room.getCheck_number()));
        holder.num_total_txt.setText(String.valueOf(room.getTotal_number()));
        int imageId = this.getMipmapResIdByName(room.getSophong());
        holder.number_room.setImageResource(imageId);
        return convertView;
    }
    // Tìm ID của Image ứng với tên của ảnh (Trong thư mục mipmap).
    public int getMipmapResIdByName(String resName)  {
        String pkgName = context.getPackageName();

        // Trả về 0 nếu không tìm thấy.
        int resID = context.getResources().getIdentifier(resName , "mipmap", pkgName);
        Log.i("CustomListView", "Res Name: "+ resName+"==> Res ID = "+ resID);
        return resID;
    }
    static class ViewHolder {
        ImageView number_room;
        TextView dr, dr_name;
        TextView num_now, num_total, num_now_txt, num_total_txt;
    }
}
