package com.example.zahuandinh.do_an_2_2;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Calendar;

public class history extends AppCompatActivity {
ListView listView;
ArrayList<String> arrayList = new ArrayList<>();
ArrayAdapter arrayAdapter;
DatabaseReference mData = FirebaseDatabase.getInstance().getReference();
Calendar calendar = Calendar.getInstance();
Intent intent;
String node_history="", check_number, room, noidung="";
int yy, mmm,dd, vitri_batdau, vitri_ketthuc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        listView = (ListView) findViewById(R.id.lv_his);
        arrayAdapter = new ArrayAdapter(history.this,android.R.layout.simple_list_item_1, arrayList);
        //Lấy ngày tháng hiên tại
        final int year = calendar.get(Calendar.YEAR);
        final int month = calendar.get(Calendar.MONTH);
        final int day = calendar.get(Calendar.DAY_OF_MONTH);
        //lấy node người dùng
        intent = getIntent();
        String node = intent.getStringExtra("node");
        mData.child("USERS").child(node).child("schedule").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                //Log.d("dudududu", dataSnapshot.getValue().toString());
                node_history = dataSnapshot.getValue().toString();
                vitri_batdau = node_history.indexOf(", check_");
                if(vitri_batdau > 0)
                {
                    yy = Integer.valueOf(node_history.substring(vitri_batdau-4,vitri_batdau));
                    //Log.d("dudududu", "year: " + yy);
                    vitri_batdau = node_history.indexOf("-");
                    mmm = Integer.valueOf(node_history.substring(vitri_batdau+1,vitri_batdau+2));
                    //Log.d("dudududu", "month: " + mmm);
                    vitri_ketthuc = node_history.indexOf("date=");
                    dd = Integer.valueOf(node_history.substring(vitri_ketthuc+5,vitri_batdau));
                    //Log.d("dudududu", "day: " + dd);
                    if(yy <= year && mmm <= (month+1) && dd < day)
                    {
                        vitri_batdau = node_history.indexOf("number=");
                        check_number = node_history.substring(vitri_batdau+7,vitri_batdau+8);
                        vitri_batdau = node_history.indexOf("room=");
                        room =  node_history.substring(vitri_batdau+5,vitri_batdau+6);
                        Log.d("dudududu", dataSnapshot.getValue().toString());
                        Log.d("dudududu", "room: " + room + "    check: " + check_number);
                        noidung = "Date: " + String.valueOf(dd) + "-" + String.valueOf(mmm) + "-" + String.valueOf(yy) + "\n" + "Your numerical order: " + check_number + "\n" + "Room: " + room;
                        arrayList.add(noidung);
                        listView.setAdapter(arrayAdapter);
                    }
                }

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
