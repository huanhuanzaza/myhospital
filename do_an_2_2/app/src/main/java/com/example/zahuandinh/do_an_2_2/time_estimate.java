package com.example.zahuandinh.do_an_2_2;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class time_estimate {
    DatabaseReference mData = FirebaseDatabase.getInstance().getReference();
    public int total_number, wait_time,w_hour,w_minute, check_number, denta_time,w_shift;
    // tính toán thời gian khám của số mới nhất được cấp phát
    public int shift(final String room)
    {
        // Ưowsc lượng thời gian khám của số thứ tự lớn nhất đã được cấp
        mData.child("ROOM").child(room).child("total_number").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                {
                    total_number = ((Long) dataSnapshot.getValue()).intValue();
                    // Phương thức lấy w_hour
                    mData.child("ROOM").child(room).child("w_hour").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if(dataSnapshot.exists())
                            {
                                w_hour =((Long) dataSnapshot.getValue()).intValue();
                                // Phương thức lấy w_minute
                                mData.child("ROOM").child(room).child("w_minute").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if(dataSnapshot.exists())
                                        {
                                            w_minute =((Long) dataSnapshot.getValue()).intValue();
                                            //Lấy số hiện tại đang được khám
                                            mData.child("ROOM").child(room).child("check_number").addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                    if(dataSnapshot.exists())
                                                    {
                                                        check_number =((Long) dataSnapshot.getValue()).intValue();
                                                        //Lấy giá trị w_wait
                                                        mData.child("ROOM").child(room).child("w_wait").addListenerForSingleValueEvent(new ValueEventListener() {
                                                            @Override
                                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                if(dataSnapshot.exists())
                                                                {
                                                                    wait_time =((Long) dataSnapshot.getValue()).intValue();
                                                                    mData.child("ROOM").child(room).child("w_shift").addListenerForSingleValueEvent(new ValueEventListener() {
                                                                        @Override
                                                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                            if(dataSnapshot.exists())
                                                                            {
                                                                                int shift = ((Long) dataSnapshot.getValue()).intValue();
                                                                                denta_time = (total_number - check_number)*wait_time;
                                                                                Log.d("break_time", "total_number : " + total_number);
                                                                                Log.d("break_time", "w_hour : " + w_hour);
                                                                                Log.d("break_time", "w_minute : " + w_minute);
                                                                                Log.d("break_time", "w_wait : " + wait_time);
                                                                                Log.d("break_time", "check_number : " + check_number);
                                                                                Log.d("break_time", "w_shift : " + shift);
                                                                                Log.d("break_time", "phong : " + room);
                                                                                w_minute += (denta_time % 60);

                                                                                if(w_minute >= 60)
                                                                                {
                                                                                    w_minute = w_minute - 60;
                                                                                    w_hour = w_hour + (denta_time/60) + 1;
                                                                                    if(w_hour >= 24)
                                                                                    {
                                                                                        w_hour = w_hour - 24;
                                                                                    }
                                                                                }
                                                                                else
                                                                                {
                                                                                    w_hour = w_hour + (denta_time/60);
                                                                                    if(w_hour >= 24)
                                                                                    {
                                                                                        w_hour = w_hour - 24;
                                                                                    }
                                                                                }
                                                                                Log.d("break_time", " HOUR : " + w_hour);
                                                                                Log.d("break_time", " MINUTE : " + w_minute);
                                                                                if(shift == 0)
                                                                                {
                                                                                    if((w_hour >= 11 && w_minute >= 30) ||(w_hour >= 17 && w_minute >= 0))
                                                                                    {
                                                                                        mData.child("ROOM").child(room).child("w_shift").setValue(total_number);
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        total_number = 0;
                                                                                    }
                                                                                }
                                                                                else if(check_number != 0 && shift >= check_number)
                                                                                {
                                                                                    mData.child("ROOM").child(room).child("w_shift").setValue(0);
                                                                                }
                                                                            }
                                                                        }

                                                                        @Override
                                                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                                                        }
                                                                    });
                                                                }
                                                            }

                                                            @Override
                                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                                            }
                                                        });
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                }
                                            });
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });
                }

            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return total_number;
    }
    public int reset_shift(final String room)
    {
        //check_number = check_number(room);
        mData.child("ROOM").child(room).child("w_shift").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists())
                {

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return 0;
    }
}
