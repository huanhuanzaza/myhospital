package com.example.zahuandinh.do_an_2_2;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class medical_file extends AppCompatActivity {
ListView listView;
Button return_button;
DatabaseReference mData = FirebaseDatabase.getInstance().getReference();
FirebaseUser user;
ArrayList <String> arrayList = new ArrayList<>();
ArrayAdapter arrayAdapter;
Intent intent;
int vitri_batdau, vitri_ketthuc;
String mode="", email="", node="",phong="", hoso="", node_hoso="",dr="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_medical_file);
        anhxa();
        user = FirebaseAuth.getInstance().getCurrentUser();
        final Animation blink = AnimationUtils.loadAnimation(this,R.anim.blink);
        arrayAdapter = new ArrayAdapter(medical_file.this,android.R.layout.simple_list_item_1,arrayList);
        intent = getIntent();
        mode = intent.getStringExtra("mode");
        if (mode.equals("0"))
        {
            email = intent.getStringExtra("email");
            phong = intent.getStringExtra("phong");
        }
        if (mode.equals("2"))
        {
            dr = intent.getStringExtra("dr_name");
            email = intent.getStringExtra("email");
            phong = intent.getStringExtra("phong");
        }
        if(mode.equals("1"))
        {
            email = user.getEmail();
            vitri_batdau = email.indexOf("@");
            node = node + email.substring(0,vitri_batdau);
            vitri_ketthuc = email.indexOf(".com");
            node = node + email.substring(vitri_batdau + 1,vitri_ketthuc);
            email = node;
        }
        //Kiểm tra xem người dùng có medical file ko
        mData.child("USERS").child(email).child("benh_an").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists() == true)
                {
                    mData.child("USERS").child(email).child("benh_an").addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                            //Log.d("edit_benhan", dataSnapshot.getValue().toString());
                            hoso = dataSnapshot.getValue().toString();
                            int count = 0;
                            Log.d("trinhtrinh", hoso);
                            //Biến str_temp dùng để tách chuỗi để cắt thành phần cần hiển thị
                            String str_temp="";
                            //CẮT NỘI DUNG NGÀY KHÁM
                            vitri_batdau = hoso.indexOf("Date=");
                            str_temp = hoso.substring(vitri_batdau);
                            vitri_ketthuc = str_temp.indexOf(",");
                            if(vitri_ketthuc == -1)
                                vitri_ketthuc = str_temp.lastIndexOf("}");
                            Log.d("hellohello","batdau" +  vitri_batdau);
                            Log.d("hellohello","ketthuc" +  vitri_ketthuc);
                            if(vitri_batdau > 0 && vitri_ketthuc >0)
                            {
                                node_hoso = "\nDate: " + str_temp.substring(5,vitri_ketthuc)+"\n" + "────────────────\n";
                                Log.d("hellohuan", "hiển thị: " + node_hoso);
                                Log.d("hellohuan", "node_hoso1: " + node_hoso);
                                //hoso = hoso.substring(vitri_ketthuc+1);
                                Log.d("hellohuan", "hồ sơ cắt: " + hoso);

                                for(int i = 1; i < hoso.length() ; i++)
                                {
                                    if(hoso.charAt(i) == '{')
                                    {
                                        count++;

                                    }
                                }
                                Log.d("hihuana", "hố: " + hoso);
                                //Biến lưu vị trí cắt chuỗi từng phòng
                                int end = -1;
                                //CẮT NỘI DỤNG TỪNG PHÒNG
                                for(int i = count; i > 0; i--)
                                {
                                    //CẮT TÊN PHÒNG
                                    vitri_batdau = hoso.indexOf("phong=");
                                    str_temp = hoso.substring(vitri_batdau);
                                    vitri_ketthuc = str_temp.indexOf(",");
                                    if(vitri_ketthuc == -1)
                                    {
                                        vitri_ketthuc = str_temp.indexOf("}");
                                    }
                                    else
                                    {
                                        if(str_temp.indexOf(",") > str_temp.indexOf("},") && str_temp.indexOf("},") != -1)
                                            vitri_ketthuc = str_temp.indexOf("},");
                                        else
                                            vitri_ketthuc = str_temp.indexOf(",");
                                    }
                                    Log.d("hihuana", "aa1: " + vitri_ketthuc);
                                    node_hoso =node_hoso + "\n" + "▶ ROOM: " + str_temp.substring(6,vitri_ketthuc)+ "\n  ";
                                    //CẮT NỘI DUNG TRIỆU CHỨNG
                                    vitri_batdau = hoso.indexOf("_chung=");
                                    str_temp = hoso.substring(vitri_batdau);
                                    vitri_ketthuc = str_temp.indexOf(",");
                                    if(vitri_ketthuc == -1)
                                    {
                                        vitri_ketthuc = str_temp.indexOf("}");
                                    }
                                    else
                                    {
                                        if(str_temp.indexOf(",") > str_temp.indexOf("},") && str_temp.indexOf("},") != -1)
                                            vitri_ketthuc = str_temp.indexOf("},");
                                        else
                                            vitri_ketthuc = str_temp.indexOf(",");
                                    }
                                    Log.d("hihuana", "aa2: " + vitri_ketthuc);
                                    node_hoso =node_hoso + "✎ Symptoms: " + str_temp.substring(7,vitri_ketthuc)+ "\n  ";
                                    //CẮT NỘI DUNG CHUẨN ĐOÁN
                                    vitri_batdau = hoso.indexOf("_doan=");
                                    str_temp = hoso.substring(vitri_batdau);
                                    vitri_ketthuc = str_temp.indexOf(",");
                                    if(vitri_ketthuc == -1)
                                    {
                                        vitri_ketthuc = str_temp.indexOf("}");
                                    }
                                    else
                                    {
                                        if(str_temp.indexOf(",") > str_temp.indexOf("},") && str_temp.indexOf("},") != -1)
                                            vitri_ketthuc = str_temp.indexOf("},");
                                        else
                                            vitri_ketthuc = str_temp.indexOf(",");
                                    }
                                    Log.d("hihuana", "aa3: " + vitri_ketthuc);
                                    node_hoso =node_hoso + "✎ Diagnostic: " + str_temp.substring(6,vitri_ketthuc)+ "\n  ";
                                    //CẮT NỘI DUNG CHỈ DẪN
                                    vitri_batdau = hoso.indexOf("chi_dan=");
                                    str_temp = hoso.substring(vitri_batdau);
                                    vitri_ketthuc = str_temp.indexOf(",");
                                    if(vitri_ketthuc == -1)
                                    {
                                        vitri_ketthuc = str_temp.indexOf("}");
                                    }
                                    else
                                    {
                                        if(str_temp.indexOf(",") > str_temp.indexOf("},") && str_temp.indexOf("},") != -1)
                                            vitri_ketthuc = str_temp.indexOf("},");
                                        else
                                            vitri_ketthuc = str_temp.indexOf(",");
                                    }
                                    Log.d("hihuana", "aa4: " + vitri_ketthuc);
                                    node_hoso =node_hoso + "✎ Instruction: " + str_temp.substring(8,vitri_ketthuc)+ "\n  ";
                                    end = hoso.indexOf("}");
                                    hoso = hoso.substring(end + 1);
                                    Log.d("edit_benhan", "END: " + end);
                                    Log.d("edit_benhan", "chuoi_Cat\n" + hoso);
                                }
                    /*
                    node_hoso =node_hoso + "- Symptoms: " + hoso.substring(vitri_batdau + 6,vitri_ketthuc)+"\n";
                    Log.d("hellohuan", "node_hoso2: " + node_hoso);
                    vitri_batdau = hoso.indexOf("doan=");
                    vitri_ketthuc = hoso.indexOf("}");
                    node_hoso =node_hoso + "- Diagnostic: " + hoso.substring(vitri_batdau + 5,vitri_ketthuc)+"\n";

                    Log.d("hellohuan", "node_hoso3: " + node_hoso);
                    vitri_batdau = hoso.indexOf("dan=");
                    vitri_ketthuc = hoso.indexOf(", phong");
                    node_hoso =node_hoso + "- Instructions: " + hoso.substring(vitri_batdau + 4,vitri_ketthuc)+"\n";
                    Log.d("hellohuan", "node_hoso4: " + node_hoso);*/
                                hoso = node_hoso;
                                Log.d("hellohuan", "hồ sơ: " + hoso);
                                arrayList.add(hoso);
                                listView.setAdapter(arrayAdapter);
                            }
                        }

                        @Override
                        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                        }

                        @Override
                        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
                else
                {
                    Toast.makeText(medical_file.this, "This Patient don't have medical file", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        if(mode.equals("1"))
        {
            return_button.setVisibility(View.INVISIBLE);
        }
        return_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mode.equals("0"))
                {
                    Intent intent = new Intent(medical_file.this, doctor.class);
                    intent.putExtra("permit","1");
                    intent.putExtra("email",email);
                    intent.putExtra("phong",phong);
                    startActivity(intent);
                    overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
                    finish();
                }
                if(mode.equals("2"))
                {
                    Intent intent = new Intent(medical_file.this, specialist.class);
                    intent.putExtra("permit","1");
                    intent.putExtra("email",email);
                    intent.putExtra("room_name",phong);
                    intent.putExtra("dr_name",dr);
                    startActivity(intent);
                    overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
                    finish();
                }
            }
        });
    }
    void anhxa()
    {
        listView = (ListView) findViewById(R.id.lv_file);
        return_button = (Button) findViewById(R.id.but_file);
    }
}
