package com.example.zahuandinh.do_an_2_2;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;

public class list_specialist extends AppCompatActivity {
ListView listView;
TextView txt_dialog;
ArrayList<String> arrayList = new ArrayList<>();
ArrayAdapter arrayAdapter;
Button sp_ok, but_yes, but_no;
String node="", noidung="", date;
int vitri_batdau, vitri_ketthuc;
Intent intent;
DatabaseReference mData = FirebaseDatabase.getInstance().getReference();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_specialist);
        // LẤY NGÀY THÁNG NĂM HIỆN TẠI
        Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mmm = calendar.get(Calendar.MONTH) + 1;
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        date = String.valueOf(dd) + "-" + String.valueOf(mmm) + "-" + String.valueOf(yy);
        final Intent i = getIntent();
        final int dieuhuong = i.getIntExtra("dieuhuong",0);
        final int patient = i.getIntExtra("patient",0);
        anhxa();
        final Animation blink = AnimationUtils.loadAnimation(this,R.anim.blink);
        arrayAdapter = new ArrayAdapter(list_specialist.this,android.R.layout.simple_list_item_1, arrayList);
        if(patient == 0)
        {
            //ĐỌC DỮ LIỆU TỪ FIREBASE
            mData.child("specialist").addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                    Log.d("kokokoko", dataSnapshot.getValue().toString());
                    String str_tremp;
                    node =  dataSnapshot.getValue().toString();
                    vitri_batdau = node.indexOf("sophong=");
                    str_tremp = node.substring(vitri_batdau);
                    Log.d("list_specialist", " node : \n" +  node);
                    if(vitri_batdau > 0)
                    {
                        vitri_ketthuc = str_tremp.indexOf(",");
                        if(vitri_ketthuc < 0)
                        {
                            vitri_ketthuc = str_tremp.indexOf("}");
                        }

                        noidung = noidung + "\n✪ ROOM: " + str_tremp.substring(8, vitri_ketthuc) + "\n";
                        // cắt doctor
                        vitri_batdau = node.indexOf("doctor=");
                        str_tremp = node.substring(vitri_batdau);
                        vitri_ketthuc = str_tremp.indexOf(",");
                        if(vitri_ketthuc < 0)
                        {
                            vitri_ketthuc = str_tremp.indexOf("}");
                        }
                        noidung = noidung + "  ▸ Doctor: " + str_tremp.substring(7, vitri_ketthuc) + "\n";
                        vitri_batdau = node.indexOf(" check_number=");
                        str_tremp = node.substring(vitri_batdau);
                        vitri_ketthuc = str_tremp.indexOf(",");
                        if(vitri_ketthuc < 0)
                        {
                            vitri_ketthuc = str_tremp.indexOf("}");
                        }

                        noidung = noidung + "  ▸ Now: " + str_tremp.substring(14, vitri_ketthuc) + "\n";

                        vitri_batdau = node.indexOf(" total_number=");
                        str_tremp = node.substring(vitri_batdau);
                        vitri_ketthuc = str_tremp.indexOf(",");
                        if(vitri_ketthuc < 0)
                        {
                            vitri_ketthuc = str_tremp.indexOf("}");
                        }
                        noidung = noidung + "  ▸ Total: " + str_tremp.substring(14, vitri_ketthuc) + "\n";

                        arrayList.add(noidung);
                        listView.setAdapter(arrayAdapter);
                        noidung = "";
                    }
                }

                @Override
                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                    Log.d("kokokoko", dataSnapshot.getValue().toString());
                }

                @Override
                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
        else if(patient == 1)
        {
            final String room = i.getStringExtra("room");
            final int[] check_number = new int[1];
            mData.child("specialist").child(room).child("check_number").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.exists())
                    {
                        check_number[0] = ((Long) dataSnapshot.getValue()).intValue();
                        mData.child("specialist").child(room).child("schedule").addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if(dataSnapshot.exists())
                                {
                                    String temp ;
                                    temp = dataSnapshot.getValue().toString().substring(dataSnapshot.getValue().toString().indexOf(date));
                                    temp = temp.substring(temp.indexOf("{")+1,temp.indexOf("}"));
                                    Log.d("list_specialist", "schedule:\n"+ temp);

                                }
                                else
                                {
                                    Log.d("list_specialist", "schedule null");
                                    return;
                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                    else
                    {
                        Log.d("list_specialist", "check_number null");
                        return;
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
        // Lấy dữ liệu từ activity doctor


        sp_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sp_ok.startAnimation(blink);
                if(dieuhuong == 0)
                {
                    intent = new Intent(list_specialist.this, admin.class);
                }
                else if(dieuhuong == 1 )
                {
                    String email = i.getStringExtra("email");
                    String phong = i.getStringExtra("phong");
                    intent = new Intent(list_specialist.this, doctor.class);
                    intent.putExtra("email", email);
                    Log.d("list_specialist", " email " + email);
                    intent.putExtra("phong", phong);
                    intent.putExtra("permit","1");
                }
                if(patient == 1)
                {
                    String email = i.getStringExtra("email");
                    String phong = i.getStringExtra("room");
                    String dr = i.getStringExtra("dr_name");
                    intent = new Intent(list_specialist.this, specialist.class);
                    intent.putExtra("permit","1");
                    intent.putExtra("email",email);
                    intent.putExtra("room_name",phong);
                    intent.putExtra("dr_name",dr);
                }
                startActivity(intent);
                overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
                finish();
            }
        });


        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, final long id) {
                if(dieuhuong == 1)
                {
                    final Dialog dialog;
                    dialog = new Dialog(list_specialist.this);
                    dialog.setTitle("Specialist");
                    dialog.setContentView(R.layout.yesno);
                    but_yes = (Button) dialog.findViewById(R.id.yes);
                    but_no = (Button) dialog.findViewById(R.id.no);
                    txt_dialog = (TextView) dialog.findViewById(R.id.topic);
                    txt_dialog.setText("Do you to take the patient to this room?");
                    dialog.show();
                    but_yes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String email, phong;
                            but_yes.startAnimation(blink);
                            if(dieuhuong == 1)
                            {
                                email = i.getStringExtra("email");
                                phong = i.getStringExtra("phong");
                                Log.d("list_specialist", " mail " + email);
                                Log.d("list_specialist", " date " + date);

                                String room_temp = arrayList.get(position);
                                room_temp = room_temp.substring(room_temp.indexOf("\n") + 1);
                                vitri_batdau = room_temp.indexOf("ROOM: ");
                                vitri_ketthuc = room_temp.indexOf("\n");
                                room_temp = room_temp.substring(vitri_batdau + 6,vitri_ketthuc);
                                Log.d("list_specialist", " Room :  " + room_temp);
                                specialist_dieuhuong.dieuhuong(email, date, room_temp);
                                Toast.makeText(list_specialist.this, "Successfully", Toast.LENGTH_SHORT).show();
                            }
                            dialog.cancel();
                        }
                    });
                    but_no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.cancel();
                        }
                    });

                }
                return false;
            }
        });
    }
    void anhxa()
    {
        listView = (ListView) findViewById(R.id.lv_sp);
        sp_ok = (Button) findViewById(R.id.ok_sp);
    }
}
