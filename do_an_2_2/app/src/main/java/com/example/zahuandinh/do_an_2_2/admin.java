package com.example.zahuandinh.do_an_2_2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupMenu;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class admin extends AppCompatActivity {
ImageView im_doctor, im_chuyenkhoa, im_tongquat, im_lichkham, im_admin, im_search, lock_usr;
Button but_admin;
Intent intent;
DatabaseReference mData = FirebaseDatabase.getInstance().getReference();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        anhxa();
        final Animation blink = AnimationUtils.loadAnimation(this,R.anim.blink);
        im_tongquat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                im_tongquat.startAnimation(blink);
                intent = new Intent(admin.this, phongkham.class);
                startActivity(intent);
                overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
            }
        });
        but_admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                but_admin.startAnimation(blink);
                intent = new Intent(admin.this, process.class);
                startActivity(intent);
                overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
            }
        });
        im_doctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                im_doctor.startAnimation(blink);
                ShowMenu();
            }
        });
        im_admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                im_admin.startAnimation(blink);
                ShowMenuadmin();
            }
        });
        im_chuyenkhoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                im_chuyenkhoa.startAnimation(blink);
                ShowMenuSpecialist();
            }
        });
        lock_usr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lock_usr.startAnimation(blink);
                intent = new Intent(admin.this, list_doctor.class);
                intent.putExtra("mode","0");
                startActivity(intent);
                overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
            }
        });
    }
    private  void ShowMenu()
    {
        PopupMenu popupMenu = new PopupMenu(this,im_doctor);
        popupMenu.getMenuInflater().inflate(R.menu.bacsi, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId())
                {
                    case R.id.list:
                    {
                        intent = new Intent(admin.this, list_doctor.class);
                        intent.putExtra("mode","2");
                        startActivity(intent);
                        overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
                        break;
                    }
                    case R.id.add_doctor:
                    {
                        intent = new Intent(admin.this, signup.class);
                        intent.putExtra("quyen","2");
                        startActivity(intent);
                        overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
                        break;
                    }
                }
                return false;
            }
        });
        popupMenu.show();
    }
    private  void ShowMenuadmin()
    {
        PopupMenu popupMenu = new PopupMenu(this,im_admin);
        popupMenu.getMenuInflater().inflate(R.menu.admin, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId())
                {
                    case R.id.list_admin:
                    {
                        intent = new Intent(admin.this, list_doctor.class);
                        intent.putExtra("mode","1");
                        startActivity(intent);
                        overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
                        finish();
                        break;
                    }
                    case R.id.add_admin:
                    {
                        intent = new Intent(admin.this, signup.class);
                        intent.putExtra("quyen","1");
                        startActivity(intent);
                        overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
                        finish();
                        break;
                    }
                }
                return false;
            }
        });
        popupMenu.show();
    }
    private  void ShowMenuSpecialist()
    {
        PopupMenu popupMenu = new PopupMenu(this,im_chuyenkhoa);
        popupMenu.getMenuInflater().inflate(R.menu.specialist, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId())
                {
                    case R.id.add_specialist:
                    {
                        intent = new Intent(admin.this, add_specialist.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
                        finish();
                        break;
                    }
                    case R.id.list_specialist:
                    {
                        intent = new Intent(admin.this, list_specialist.class);
                        startActivity(intent);
                        overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
                        finish();
                        break;
                    }
                }
                return false;
            }
        });
        popupMenu.show();
    }
    void anhxa()
    {
        im_doctor = (ImageView) findViewById(R.id.doctor);
        im_chuyenkhoa = (ImageView) findViewById(R.id.chuyenkhoa);
        im_tongquat = (ImageView) findViewById(R.id.tongquat);
        im_admin = (ImageView) findViewById(R.id.admin);
        im_search = (ImageView) findViewById(R.id.timkiem);
        but_admin = (Button) findViewById(R.id.admin_but);
        lock_usr = (ImageView) findViewById(R.id.lock);
    }
}
