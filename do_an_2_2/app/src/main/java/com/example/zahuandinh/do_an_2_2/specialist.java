package com.example.zahuandinh.do_an_2_2;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

public class specialist extends AppCompatActivity {
TextView sp_name,sp_now, sp_total, sp_dr;
ImageView qr_sp, write_sp, schudule_sp, file_sp, w_patient;
Button next_sp;
Intent intent;
String dr_name="", room_name="", email="", node_mail="",permit;
int thutu,total_number, vitri_batdau, vitri_ketthuc;
DatabaseReference mData = FirebaseDatabase.getInstance().getReference();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_specialist);
        anhxa();
        final Animation blink = AnimationUtils.loadAnimation(this,R.anim.blink);
        final IntentIntegrator i = new IntentIntegrator(this);
        //Lấy thông tin tên bs, tên phòng
        intent = getIntent();
        dr_name = intent.getStringExtra("dr_name");
        room_name = intent.getStringExtra("room_name");
        permit = intent.getStringExtra("permit");
        if(permit.equals("1"))
        {
            node_mail = intent.getStringExtra("email");
        }
        //VÔ hiệu 2 nút ghi bênh án và xem bệnh án dựa trên biến permit
        if(permit.equals("0"))
        {
            write_sp.setEnabled(false);
            file_sp.setEnabled(false);
        }
        else
        {
            write_sp.setEnabled(true);
            file_sp.setEnabled(true);
        }

        //Hiển thị tên bs, tên phòng
        sp_name.setText(room_name);
        sp_dr.setText("Dr. " + dr_name);
        //Cập nhật tên bác sĩ đang khám ở phòng hiện tại
        mData.child("specialist").child(room_name).child("doctor").setValue(dr_name);
        //Hiển thị số khám hiện tại
        mData.child("specialist").child(room_name).child("check_number").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                sp_now.setText("NOW: " + dataSnapshot.getValue().toString());
                thutu = ((Long) dataSnapshot.getValue()).intValue();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        //Hiển thị số bệnh nhân hiện tại
        mData.child("specialist").child(room_name).child("total_number").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                sp_total.setText("TOTAL: " + dataSnapshot.getValue().toString());
                total_number =  ((Long) dataSnapshot.getValue()).intValue();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        //quét tên bệnh nhân
        qr_sp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                qr_sp.startAnimation(blink);
                i.initiateScan();
            }
        });
        //Xử lý nút Next
        next_sp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next_sp.startAnimation(blink);
                if(thutu == total_number)
                {
                    Toast.makeText(specialist.this, "No Patients !!!",Toast.LENGTH_LONG).show();
                }
                else
                {
                    thutu++;
                    mData.child("specialist").child(room_name).child("check_number").setValue(thutu);
                }
                write_sp.setEnabled(false);
                file_sp.setEnabled(false);
                node_mail = "";
                permit = "0";
            }
        });
        //Xử lý nút xem bệnh án
        file_sp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                file_sp.startAnimation(blink);
                Intent intent = new Intent(specialist.this,medical_file.class);
                intent.putExtra("email",node_mail);
                intent.putExtra("phong",room_name);
                intent.putExtra("dr_name",dr_name);
                intent.putExtra("mode","2");
                startActivity(intent);
                overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
                finish();
            }
        });
        write_sp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                write_sp.startAnimation(blink);
                Intent intent = new Intent(specialist.this,write_medical.class);
                intent.putExtra("email",node_mail);
                intent.putExtra("phong",room_name);
                intent.putExtra("dr_name",dr_name);
                intent.putExtra("mode","2");
                startActivity(intent);
                overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
                finish();
            }
        });
        w_patient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(specialist.this, list_specialist.class);
                intent.putExtra("patient",1);
                intent.putExtra("room",room_name);
                intent.putExtra("email",node_mail);
                intent.putExtra("dr_name",dr_name);
                startActivity(intent);
                overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
            }
        });


    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null)
        {
            if(result.getContents() == null) {
                Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            }
            else
            {
                //Picasso.get().load(result.getContents()).into(qr_i);
                try {
                    JSONObject jsonObject = new JSONObject(result.getContents());
                    email = jsonObject.getString("name");
                    Log.d("hahahaha", "email: " + email);
                    //Cắt email bệnh nhân
                    vitri_batdau = email.indexOf("@");
                    node_mail = node_mail + email.substring(0,vitri_batdau);
                    vitri_ketthuc = email.indexOf(".com");
                    node_mail = node_mail + email.substring(vitri_batdau + 1,vitri_ketthuc);
                    write_sp.setEnabled(true);
                    file_sp.setEnabled(true);
                    Toast.makeText(specialist.this, "Scan Patient " + email + " Successfully", Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        else
        {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
    void anhxa()
    {
        w_patient = (ImageView) findViewById(R.id.patient);
        sp_name = (TextView) findViewById(R.id.sp_name);
        sp_now = (TextView) findViewById(R.id.sp_now);
        sp_total = (TextView) findViewById(R.id.sp_total);
        sp_dr = (TextView) findViewById(R.id.sp_dr);
        qr_sp = (ImageView) findViewById(R.id.sp_qr);
        write_sp = (ImageView) findViewById(R.id.sp_write);
        schudule_sp = (ImageView) findViewById(R.id.sp_calendar);
        file_sp = (ImageView) findViewById(R.id.sp_file);
        next_sp = (Button) findViewById(R.id.sp_next);
    }
}
