package com.example.zahuandinh.do_an_2_2;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

public class get_num extends AppCompatActivity {
DatePicker datePicker;
Button btn_day;
String node, email="", email_node="";
int yy,mmm,dd, vitri_batdau, vitri_ketthuc, nextroom, soluong, total_number, soluong_schedule, i =0,stt=0;
DatabaseReference mData = FirebaseDatabase.getInstance().getReference();
FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_num);
        anhxa();
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(calendar.YEAR);
        yy = year;
        int month = calendar.get(calendar.MONTH);
        mmm = month + 1;
        final int day = calendar.get(calendar.DAY_OF_MONTH);
        dd = day;
        //VÔ HIỆU HÓA NHỮNG NGÀY TRONG QUÁ KHỨ
        datePicker.setMinDate(System.currentTimeMillis() - 1000);

        Log.d("date_date","Millisecond: " + String.valueOf(System.currentTimeMillis()));
        Log.d("date_date", String.valueOf(new Date().getTime()));
        // THIẾT LẬP THỜI GIAN CÓ THỂ ĐẶT LỊCH KHÁM LÀ 2 TUẦN TRONG TƯƠNG LAI
        // THAM SỐ TRUYỀN VÀO LÀ ĐƠN VỊ MILL GIÂY, KHOẢNG MILLI GIÂY
        // HÀM System.currentTimeMillis() TRẢ VỀ SỐ MILL GIÂY CỦA HIỆN TẠI ( SO VỚI NGÀY 1 - 1 -1970)
        // TÍNH KHOẢNG MILI GIÂY CỦA 2 TUẦN RỒI CỘNG THÊM (86400000 MILI GIÂY/ NGÀY)
        //* NHỚ TRỪ 1000 MILI GIÂY
        datePicker.setMaxDate(System.currentTimeMillis()+1209599000);
        email = user.getEmail();
        vitri_batdau = email.indexOf("@");
        email_node = email_node + email.substring(0,vitri_batdau);
        vitri_ketthuc = email.indexOf(".com");
        email_node = email_node + email.substring(vitri_batdau + 1,vitri_ketthuc);
        Log.d("hihuan", "Email_node : " + email_node);
        mData.child("SCHEDULE").child("soluong").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                soluong_schedule = ((Long) dataSnapshot.getValue()).intValue();
                Log.d("hihuan", "soluong_schedule : " + soluong_schedule);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        setupDatePicker();
    }
    void setupDatePicker()
    {
        final Animation blink = AnimationUtils.loadAnimation(this,R.anim.blink);
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(calendar.YEAR);
        yy = year;
        int month = calendar.get(calendar.MONTH);
        mmm = month + 1;
        final int day = calendar.get(calendar.DAY_OF_MONTH);
        dd = day;

        datePicker.init(year, month, day, new DatePicker.OnDateChangedListener() {
            @Override
            public void onDateChanged(final DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                monthOfYear = monthOfYear + 1;
                // Lấy ngày tháng năm được chọn từ DATEPICKER
                node = dayOfMonth + "-" + monthOfYear + "-" + year;
                //Nếu chọn ngay ngày hôm nay
                if(year == yy && monthOfYear == mmm && dayOfMonth == dd)
                {
                    btn_day.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            btn_day.startAnimation(blink);
                            mData.child("USERS").child(email_node).child("phat").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    int phat = ((Long) dataSnapshot.getValue()).intValue();
                                    if(dataSnapshot.exists() == true)
                                    {
                                        Log.d("phat_tien", "phat = " + phat);
                                        if(phat >= 3)
                                        {
                                            Toast.makeText(get_num.this,"You are locked because you were absent 3 times", Toast.LENGTH_LONG).show();
                                        }
                                        else
                                        {
                                            Log.d("hihuan", "node hiện tại : " + node);
                                            //Lấy số lượng phòng tối đa ngày hôm nay
                                            mData.child("ROOM").child("soluong").addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                    soluong = ((Long) dataSnapshot.getValue()).intValue();
                                                    //Lấy giá trị phòng kế tiếp để tìm tổng số phiếu đã xuất của phòng đó
                                                    mData.child("ROOM").child("nextroom").addListenerForSingleValueEvent(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                            nextroom = ((Long) dataSnapshot.getValue()).intValue();
                                                            // lấy giá trị tổng số phiếu của phòng đó
                                                            mData.child("ROOM").child(String.valueOf(nextroom)).child("total_number").addListenerForSingleValueEvent(new ValueEventListener() {
                                                                @Override
                                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                    total_number = ((Long) dataSnapshot.getValue()).intValue();
                                                                    //DỮ LIỆU THÊM VÀO NGÀY 24-3
                                                                    mData.child("SCHEDULE").child(node).child("date").setValue(node);
                                                                    mData.child("SCHEDULE").child(node).child(String.valueOf(nextroom)).child(String.valueOf(total_number + 1)).setValue(email_node);
                                                                    mData.child("SCHEDULE").child(node).child(String.valueOf(nextroom)).child("total_number").setValue(total_number+1);
                                                                    //thêm giá trị số lượng phòng được đặt số
                                                                    mData.child("SCHEDULE").child(node).child("soluong").addListenerForSingleValueEvent(new ValueEventListener() {
                                                                        @Override
                                                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                            if(dataSnapshot.exists() == false)
                                                                            {
                                                                                mData.child("SCHEDULE").child(node).child("soluong").setValue(1);
                                                                            }
                                                                            else
                                                                            {
                                                                                mData.child("SCHEDULE").child(node).child("soluong").setValue(((Long) dataSnapshot.getValue()).intValue() + 1);
                                                                            }
                                                                        }

                                                                        @Override
                                                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                                                        }
                                                                    });
                                                                    //Cập nhật lịch đặt khám cho bệnh nhân trong dữ liệu của bệnh nhân
                                                                    mData.child("USERS").child(email_node).child("schedule").child(node).child("date").setValue(node);
                                                                    mData.child("USERS").child(email_node).child("schedule").child(node).child("check_number").setValue(total_number + 1);
                                                                    mData.child("USERS").child(email_node).child("schedule").child(node).child("room").setValue(String.valueOf(nextroom));
                                                                    mData.child("ROOM").child(String.valueOf(nextroom)).child("total_number").setValue(total_number+1);
                                                                    // mData.child("ROOM").child(String.valueOf(nextroom)).child("total_number").setValue(total_number+1);
                                                                    //kiểm tra giá trị next room có bằng số phòng lớn nhất hay chưa --> nếu rồi thì set lại về phòng 1
                                                                    if(nextroom == soluong)
                                                                    {
                                                                        mData.child("ROOM").child("nextroom").setValue(1);
                                                                    }
                                                                    else
                                                                    {
                                                                        mData.child("ROOM").child("nextroom").setValue(nextroom+1);
                                                                        mData.child("SCHEDULE").child(node).child("nextroom").setValue(nextroom+1);
                                                                    }
                                                                    Intent intent = new Intent(get_num.this, process.class);
                                                                    startActivity(intent);
                                                                    overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
                                                                    String bip = "bip.mp3";

                                                                    MediaPlayer mp=new MediaPlayer();
                                                                    try{
                                                                        mp.setDataSource("https://notificationsounds.com/notification-sounds/definite-555#play");//Write your location here
                                                                        mp.prepare();
                                                                        mp.start();

                                                                    }catch(Exception e){e.printStackTrace();

                                                                }

                                                                    Toast.makeText(get_num.this,"Getting Numerical Order Successfully" ,Toast.LENGTH_LONG).show();
                                                                }

                                                                @Override
                                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                                }
                                                            });
                                                        }

                                                        @Override
                                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                                        }
                                                    });
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                }
                                            });
                                        }
                                    }
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                        }
                    });

                }
                //Nếu chọn khác ngày hôm nay
                else {

                    btn_day.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v)
                        {
                            btn_day.startAnimation(blink);
                            // KIỂM TRA NGƯỜI DÙNG CÓ BỊ PHẠT KHÔNG
                            mData.child("USERS").child(email_node).child("phat").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    int phat = ((Long) dataSnapshot.getValue()).intValue();
                                    if(dataSnapshot.exists() == true)
                                    {
                                        if(phat == 3)
                                        {
                                            Toast.makeText(get_num.this,"You are locked because you were absent 3 times", Toast.LENGTH_LONG).show();
                                        }
                                        else
                                        {
                                            mData.child("SCHEDULE").addChildEventListener(new ChildEventListener() {
                                                @Override
                                                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                                                    Log.d("hihuan", "du lieu được chọn : " + dataSnapshot.getValue().toString());
                                                    //TRƯỜNG HỢP KHI CHƯA CÓ NGÀY NÀO ĐƯỢC ĐẶT LỊCH
                                                    if(soluong_schedule == 0)
                                                    {
                                                        mData.child("SCHEDULE").child(node).child("date").setValue(node);
                                                        mData.child("SCHEDULE").child(node).child("1").child("total_number").setValue(1);
                                                        mData.child("SCHEDULE").child(node).child("1").child("1").setValue(email);
                                                        mData.child("SCHEDULE").child(node).child("nextroom").setValue(2);
                                                        mData.child("SCHEDULE").child(node).child("soluong").setValue(1);
                                                        mData.child("SCHEDULE").child("soluong").setValue(soluong_schedule+1);
                                                        mData.child("USERS").child(email_node).child("schedule").child(node).child("date").setValue(node);
                                                        mData.child("USERS").child(email_node).child("schedule").child(node).child("check_number").setValue(1);
                                                        mData.child("USERS").child(email_node).child("schedule").child(node).child("room").setValue("1");
                                                        MediaPlayer mp=new MediaPlayer();
                                                        try{
                                                            mp.setDataSource("https://notificationsounds.com/notification-sounds/definite-555#play");//Write your location here
                                                            mp.prepare();
                                                            mp.start();

                                                        }catch(Exception e){e.printStackTrace();

                                                        }
                                                        Intent intent = new Intent(get_num.this, process.class);
                                                        startActivity(intent);
                                                        overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
                                                        Toast.makeText(get_num.this,"Getting Numerical Order Successfully" ,Toast.LENGTH_LONG).show();
                                                    }
                                                    // TRƯỜNG HỢP ĐÃ CÓ NGÀY ĐƯỢC ĐẶT LỊCH
                                                    else
                                                    {
                                                        vitri_batdau = (dataSnapshot.getValue().toString()).indexOf(node);
                                                        if(i < soluong_schedule)
                                                        {
                                                            if(vitri_batdau > 0)
                                                                stt = 1;
                                                            i++;
                                                        }
                                                        if(i == soluong_schedule)
                                                        {
                                                            //NẾU NGÀY ĐƯỢC CHỌN CHƯA CÓ AI ĐẶT LỊCH KHÁM
                                                            if(stt == 0)
                                                            {
                                                                Log.d("hihuan", "TAO MOI 1 SCHEDULE");
                                                                mData.child("SCHEDULE").child(node).child("date").setValue(node);
                                                                mData.child("SCHEDULE").child(node).child("1").child("total_number").setValue(1);
                                                                mData.child("SCHEDULE").child(node).child("1").child("1").setValue(email);
                                                                mData.child("SCHEDULE").child(node).child("nextroom").setValue(2);
                                                                mData.child("SCHEDULE").child(node).child("soluong").setValue(1);
                                                                mData.child("SCHEDULE").child("soluong").setValue(soluong_schedule+1);
                                                                mData.child("USERS").child(email_node).child("schedule").child(node).child("date").setValue(node);
                                                                mData.child("USERS").child(email_node).child("schedule").child(node).child("check_number").setValue(1);
                                                                mData.child("USERS").child(email_node).child("schedule").child(node).child("room").setValue("1");
                                                                Intent intent = new Intent(get_num.this, process.class);
                                                                startActivity(intent);
                                                                overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
                                                                MediaPlayer mp=new MediaPlayer();
                                                                try{
                                                                    mp.setDataSource("https://notificationsounds.com/notification-sounds/definite-555#play");//Write your location here
                                                                    mp.prepare();
                                                                    mp.start();

                                                                }catch(Exception e){e.printStackTrace();

                                                                }
                                                                Toast.makeText(get_num.this,"Getting Numerical Order Successfully" ,Toast.LENGTH_LONG).show();
                                                            }
                                                            // NẾU NGÀY ĐƯỢC CHỌN ĐÃ ĐƯỢC ĐẶT
                                                            else
                                                            {
                                                                mData.child("SCHEDULE").child(node).child("soluong").addListenerForSingleValueEvent(new ValueEventListener() {
                                                                    @Override
                                                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                        soluong = ((Long) dataSnapshot.getValue()).intValue();
                                                                        if(soluong < 7 )
                                                                        {
                                                                            mData.child("SCHEDULE").child(node).child(String.valueOf(soluong+1)).child("total_number").setValue(1);
                                                                            mData.child("SCHEDULE").child(node).child(String.valueOf(soluong+1)).child("1").setValue(email);
                                                                            mData.child("SCHEDULE").child(node).child("soluong").setValue(soluong+1);
                                                                            mData.child("USERS").child(email_node).child("schedule").child(node).child("date").setValue(node);
                                                                            mData.child("USERS").child(email_node).child("schedule").child(node).child("check_number").setValue(1);
                                                                            mData.child("USERS").child(email_node).child("schedule").child(node).child("room").setValue("1");
                                                                            if(soluong == 6)
                                                                            {
                                                                                mData.child("SCHEDULE").child(node).child("nextroom").setValue(1);
                                                                            }
                                                                            else
                                                                            {
                                                                                mData.child("SCHEDULE").child(node).child("nextroom").setValue(soluong+2);
                                                                            }
                                                                            Intent intent = new Intent(get_num.this, process.class);
                                                                            startActivity(intent);
                                                                            overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
                                                                            MediaPlayer mp=new MediaPlayer();
                                                                            try{
                                                                                mp.setDataSource("https://notificationsounds.com/notification-sounds/definite-555#play");//Write your location here
                                                                                mp.prepare();
                                                                                mp.start();

                                                                            }catch(Exception e){e.printStackTrace();

                                                                            }
                                                                            Toast.makeText(get_num.this,"Getting Numerical Order Successfully" ,Toast.LENGTH_LONG).show();
                                                                        }
                                                                        if(soluong == 7)
                                                                        {
                                                                            mData.child("SCHEDULE").child(node).child("nextroom").addListenerForSingleValueEvent(new ValueEventListener() {
                                                                                @Override
                                                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                                    nextroom = ((Long) dataSnapshot.getValue()).intValue();
                                                                                    mData.child("SCHEDULE").child(node).child(String.valueOf(nextroom)).child("total_number").addListenerForSingleValueEvent(new ValueEventListener() {
                                                                                        @Override
                                                                                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                                                            total_number = ((Long) dataSnapshot.getValue()).intValue();
                                                                                            mData.child("SCHEDULE").child(node).child(String.valueOf(nextroom)).child("total_number").setValue(total_number+1);
                                                                                            mData.child("SCHEDULE").child(node).child(String.valueOf(nextroom)).child(String.valueOf(total_number+1)).setValue(email);
                                                                                            if(nextroom == 7)
                                                                                            {
                                                                                                mData.child("SCHEDULE").child(node).child("nextroom").setValue(1);
                                                                                            }
                                                                                            else
                                                                                            {
                                                                                                mData.child("SCHEDULE").child(node).child("nextroom").setValue(nextroom+1);
                                                                                            }
                                                                                            mData.child("USERS").child(email_node).child("schedule").child(node).child("date").setValue(node);
                                                                                            mData.child("USERS").child(email_node).child(node).child("check_number").setValue(total_number+1);
                                                                                            mData.child("USERS").child(email_node).child(node).child("room").setValue(nextroom);
                                                                                            Intent intent = new Intent(get_num.this, process.class);
                                                                                            startActivity(intent);
                                                                                            overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
                                                                                            MediaPlayer mp=new MediaPlayer();
                                                                                            try{
                                                                                                mp.setDataSource("https://notificationsounds.com/notification-sounds/definite-555#play");//Write your location here
                                                                                                mp.prepare();
                                                                                                mp.start();

                                                                                            }catch(Exception e){e.printStackTrace();

                                                                                            }
                                                                                            Toast.makeText(get_num.this,"Getting Numerical Order Successfully" ,Toast.LENGTH_LONG).show();
                                                                                        }

                                                                                        @Override
                                                                                        public void onCancelled(@NonNull DatabaseError databaseError) {

                                                                                        }
                                                                                    });

                                                                                }

                                                                                @Override
                                                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                                                }
                                                                            });
                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                                                    }
                                                                });
                                                            }
                                                        }
                                                    }
                                                }
                                                @Override
                                                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                                                }

                                                @Override
                                                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                                                }

                                                @Override
                                                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                }
                                            });
                                        }
                                    }
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });

                        }
                    });
                }
            }
        });
    }
    void anhxa()
    {
        datePicker = (DatePicker) findViewById(R.id.time_date);
        btn_day = (Button) findViewById( R.id.get_otherday);
    }
}
