package com.example.zahuandinh.do_an_2_2;


import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;


/**
 * A simple {@link Fragment} subclass.
 */
public class tab2_getnumber extends Fragment {
View view;
TextView txt_date;
static TextView  txt_appointment;
ListView listView;
String date_time="", email="", node="", noidung="";
int vitri_batdau, vitri_ketthuc, yy,mmm,dd, room, check_number, denta_check_number, hour, minute, today_number,today_room, shift_number = 0;
double wait_time;
ArrayList<String> arrayList = new ArrayList<>();
ArrayAdapter arrayAdapter;
Button btn;
DatabaseReference mData = FirebaseDatabase.getInstance().getReference();
FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
Intent intent;
time_estimate time_es = new time_estimate();
    public tab2_getnumber() {
        // Required empty public constructor
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tab2_getnumber,container,false);
        // Inflate the layout for this fragment
        anhxa();
        final Animation blink = AnimationUtils.loadAnimation(getContext(),R.anim.blink);
        arrayAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_list_item_1, arrayList);
        //LẤY EMAIL HIỆN TẠI
        email = user.getEmail();
        //Kiểm tra xem người dùng đã xác nhận email chưa.
        final boolean emailVerified = user.isEmailVerified();
        Log.d("phat_tien", "xac nhan: " + emailVerified);
        //CẮT CHUỖI ĐỂ TÌM NODE
        vitri_batdau = email.indexOf("@");
        node = node + email.substring(0,vitri_batdau);
        vitri_ketthuc = email.indexOf(".com");
        node = node + email.substring(vitri_batdau + 1,vitri_ketthuc);
        //Lấy thông tin của Ngày Tháng Năm hiện tại
        Calendar calendar = Calendar.getInstance();
        final int year = calendar.get(calendar.YEAR);
        int month = calendar.get(calendar.MONTH);
        month = month + 1;
        final int day = calendar.get(calendar.DAY_OF_MONTH);
        date_time = day + "-" + month + "-" + year;
        txt_date.setText(day + " / " + month+ " / " + year);
        final int finalMonth = month;



        mData.child("USERS").child(node).child("schedule").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                //cắt nội dung năm
                vitri_batdau = (dataSnapshot.getValue().toString()).indexOf(", check_number");
                if(vitri_batdau > 0)
                {
                    yy = Integer.valueOf(dataSnapshot.getValue().toString().substring(vitri_batdau-4,vitri_batdau));
                    //cắt nội dung tháng
                    vitri_batdau = (dataSnapshot.getValue().toString()).indexOf("-");
                    mmm = Integer.valueOf(dataSnapshot.getValue().toString().substring(vitri_batdau+1,vitri_batdau+2));
                    //cắt nội dung năm
                    vitri_batdau = (dataSnapshot.getValue().toString()).indexOf("date=");
                    vitri_ketthuc = (dataSnapshot.getValue().toString()).indexOf("-");
                    dd = Integer.valueOf(dataSnapshot.getValue().toString().substring(vitri_batdau+5,vitri_ketthuc));
                    //cắt nội dung về số khám bệnh
                    vitri_batdau = (dataSnapshot.getValue().toString()).indexOf("check_number=");
                    vitri_ketthuc = (dataSnapshot.getValue().toString()).indexOf(", room");
                    check_number = Integer.valueOf(dataSnapshot.getValue().toString().substring(vitri_batdau+13,vitri_ketthuc));
                    //cắt nội dung về phòng khám
                    vitri_batdau = (dataSnapshot.getValue().toString()).indexOf("room=");
                    room = Integer.valueOf(dataSnapshot.getValue().toString().substring(vitri_batdau+5,vitri_batdau+6));

                    //Lấy giá trị w_shift
                    mData.child("ROOM").child(String.valueOf(room)).child("w_shift").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if(dataSnapshot.exists())
                            {
                                shift_number = ((Long) dataSnapshot.getValue()).intValue();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                    //Lấy số khám, phòng khám của hôm nay nếu có lịch khám hôm nay
                    if(yy == year && mmm == finalMonth && dd == day)
                    {
                        today_number = check_number;
                        today_room = room;
                    }
                    if(yy >= year && mmm >= finalMonth && dd >= day )
                    {
                        //Xử lý nội dung hiển thị/tính toán nếu hôm nay có lịch khám
                        if(yy == year && mmm == finalMonth && dd == day)
                        {
                            mData.child("ROOM").child(String.valueOf(today_room)).child("flag").addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                                    if (dataSnapshot.exists())
                                    {
                                        //GỌI HÀM DỰ ĐOÁN GIỜ KHÁM
                                        int flag = Integer.parseInt(dataSnapshot.getValue().toString());
                                        patient_estimate.estimate(node, today_room, today_number,flag);
                                    }

                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                            // THIẾT LẬP TEXTVIEW NẾU TỚI SỐ KHÁM
                            mData.child("ROOM").child(String.valueOf(room)).child("check_number").addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.exists())
                                    {
                                        int thutu = ((Long) dataSnapshot.getValue()).intValue();
                                        if(today_number == thutu)
                                        {
                                            tab2_getnumber.txt_appointment.setText("COMING INTO ROOM " + room + " PLEASE !" );
                                            tab2_getnumber.txt_appointment.setTextColor(Color.parseColor("#DF0029"));
                                        }
                                        else if(today_number < thutu)
                                        {
                                            tab2_getnumber.txt_appointment.setText("THANK YOU !" );
                                            tab2_getnumber.txt_appointment.setTextColor(Color.parseColor("#DF0029"));
                                        }

                                    }
                                    else
                                    {
                                        Log.d("tab2_getnumber", "check_number null");
                                        return;
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                        }
                        noidung = "✺ Your Medical Examination Schedule On " + String.valueOf(dd) + "- " + String.valueOf(mmm) + "- "  + String.valueOf(yy) + "\n" + "     ▹ At Room: " + String.valueOf(room) + "\n" + "     ▹ Your numerical order: " + String.valueOf(check_number);
                        arrayList.add(noidung);
                        listView.setAdapter(arrayAdapter);
                    }
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                Log.d("hahahuan", "change node: " + dataSnapshot.getValue().toString());

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });




        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn.startAnimation(blink);
                //Khống chế chức năng nếu chưa xác nhậ email.
                if(emailVerified == true)
                {
                    intent = new Intent(getContext(),get_num.class);
                    startActivity(intent);
                    getActivity().finish();
                }
                else
                {
                    Toast.makeText(getContext(),"Check your email to verify your account !", Toast.LENGTH_LONG).show();
                }
            }
        });
        return view;
    }
    void anhxa()
    {
        txt_date = (TextView) view.findViewById(R.id.date_txt);
        txt_appointment = (TextView) view.findViewById(R.id.note_txt);
        listView = (ListView) view.findViewById(R.id.lv_getnum);
        btn = (Button) view.findViewById(R.id.btn_getnum);
    }
}


/*
//Lấy thông tin check_number của phòng mà bệnh nhân sẽ khám hôm nay để tính thời gian đợi
                            mData.child("ROOM").child(String.valueOf(today_room)).child("check_number").addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if(dataSnapshot.exists() == true)
                                    {
                                        //TÍNH THỜI GIAN CHỜ, NẾU NGƯỜI CÓ SỐ 1 THÌ THỜI GIAN KHÁM SẼ LÀ
                                        if(shift_number == 0)
                                        {
                                            denta_check_number = today_number - ((Long) dataSnapshot.getValue()).intValue();
                                        }
                                        else
                                        {
                                            if(today_number == shift_number)
                                            {
                                                denta_check_number = 0;
                                            }
                                            else if (today_number > shift_number)
                                            {
                                                denta_check_number = today_number - shift_number;
                                            }
                                        }
                                        //Lấy mốc thời gian khám của bệnh nhân đang khám ở phòng đó
                                        mData.child("ROOM").child(String.valueOf(today_room)).child("w_hour").addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                if(dataSnapshot.exists() == true)
                                                {
                                                    hour = ((Long) dataSnapshot.getValue()).intValue();
                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                            }
                                        });
                                        mData.child("ROOM").child(String.valueOf(today_room)).child("w_minute").addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                if(dataSnapshot.exists() == true)
                                                {
                                                    minute = ((Long) dataSnapshot.getValue()).intValue();
                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                            }
                                        });
                                        //Lấy giá trị thời gian chờ đơn vị-realtime
                                        mData.child("ROOM").child(String.valueOf(today_room)).child("w_wait").addValueEventListener(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                if(dataSnapshot.exists() == true)
                                                {
                                                    wait_time = ((double) dataSnapshot.getValue())*denta_check_number;
                                                    if(shift_number == 0)
                                                    {
                                                        minute = minute + (wait_time%60);
                                                        if(minute >= 60)
                                                        {
                                                            minute = minute - 60;
                                                            hour = hour + (wait_time/60) + 1;
                                                            if(hour >= 24)
                                                            {
                                                                hour = hour - 24;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            hour = hour + (wait_time/60);
                                                            if(hour >= 24)
                                                            {
                                                                hour = hour - 24;
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        minute = 0 + (wait_time%60);
                                                        if(minute >= 60)
                                                        {
                                                            minute = minute - 60;
                                                            hour = 13 + (wait_time/60) + 1;
                                                            if(hour >= 24)
                                                            {
                                                                hour = hour - 24;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            hour = 13 + (wait_time/60);
                                                            if(hour >= 24)
                                                            {
                                                                hour = hour - 24;
                                                            }
                                                        }
                                                    }
                                                    Log.d("thoi gian kham","hour: " + hour + "  minute " + minute);
                                                    txt_appointment.setText("Your Appointment at "+ hour + " : " + minute);
                                                    //THAY ĐỔI MÀU CHỮ CHO THÔNG BÁO LỊCH KHÁM
                                                    txt_appointment.setTextColor(Color.parseColor("#DF0029"));
                                                    Log.d("thoi gian kham","năm : " + yy + " tháng : " + mmm + " ngày : " + dd + " phòng : " + room + " số khám : " + check_number );
                                                }
                                            }

                                            @Override
                                            public void onCancelled(@NonNull DatabaseError databaseError) {

                                            }
                                        });
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
 */