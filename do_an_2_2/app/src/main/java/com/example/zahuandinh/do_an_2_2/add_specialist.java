package com.example.zahuandinh.do_an_2_2;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.zxing.WriterException;

import java.io.ByteArrayOutputStream;

import androidmads.library.qrgenearator.QRGContents;
import androidmads.library.qrgenearator.QRGEncoder;

public class add_specialist extends AppCompatActivity {
EditText edit_sp;
Button btn_sp,show_qr_ok;
Dialog dialog;
ImageView img_sp;
TextView txt_sp;
Bitmap bitmap;
QRGEncoder qrgEncoder;
DatabaseReference mData = FirebaseDatabase.getInstance().getReference();
FirebaseStorage storage = FirebaseStorage.getInstance();
final StorageReference storageRef = storage.getReference();
final String[] inputValue = new String[1];
room_shownumber room;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_specialist);
        anhxa();
        //ẩn hai đối tượng: Hình ảnh mã QR và text view
        btn_sp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edit_sp.getText().toString().equals(""))
                {
                    Toast.makeText(add_specialist.this, "Type Specialist's Name !!!", Toast.LENGTH_LONG).show();
                }
                else
                {
                    inputValue[0] = ("{"+"\""+"room" + "\"" + ":" +"\""+ (edit_sp.getText().toString().replaceAll("\\s+",""))+"\""+ "}").trim();
                    if (inputValue[0].length() > 0) {
                        WindowManager manager = (WindowManager) getSystemService(WINDOW_SERVICE);
                        Display display = manager.getDefaultDisplay();
                        Point point = new Point();
                        display.getSize(point);
                        int width = point.x;
                        int height = point.y;
                        int smallerDimension = width < height ? width : height;
                        smallerDimension = smallerDimension * 3 / 4;

                        qrgEncoder = new QRGEncoder(
                                inputValue[0], null,
                                QRGContents.Type.TEXT,
                                smallerDimension);
                        try {
                            show_qr();
                            bitmap = qrgEncoder.encodeAsBitmap();
                            img_sp.setImageBitmap(bitmap);
                            //txt_sp.setText(edit_sp.getText().toString() + " 's QR");
                            dialog.show();

                            //Lưu ảnh vào Firebase Storage
                            StorageReference mountainsRef = storageRef.child(edit_sp.getText().toString().replaceAll("\\s+","") + ".png");
                            img_sp.setDrawingCacheEnabled(true);
                            img_sp.buildDrawingCache();
                            Bitmap bitmap = ((BitmapDrawable) img_sp.getDrawable()).getBitmap();
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                            byte[] data = baos.toByteArray();

                            UploadTask uploadTask = mountainsRef.putBytes(data);
                            uploadTask.addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    Toast.makeText(add_specialist.this, " Save QR Failed ", Toast.LENGTH_LONG).show();
                                }
                            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    Toast.makeText(add_specialist.this, "Save Successfully", Toast.LENGTH_LONG).show();
                                }
                            });
                            room = new room_shownumber("No Doctor",edit_sp.getText().toString().replaceAll("\\s+",""),0,0);
                            mData.child("specialist").child(edit_sp.getText().toString().replaceAll("\\s+","")).setValue(room);
                            //Nhấn nút OK để trở về màn hình trước
                            show_qr_ok.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    finish();
                                }
                            });
                        } catch (WriterException e) {

                        }
                    } else {
                        //edtValue.setError("Required");
                    }
                }
            }
        });
    }
    void show_qr()
    {
        dialog = new Dialog(add_specialist.this);
        dialog.setTitle("QR CODE");
        dialog.setContentView(R.layout.show_qrcode);
        img_sp = (ImageView) dialog.findViewById(R.id.im_qr);
        show_qr_ok = (Button) dialog.findViewById(R.id.save_qr);
    }
    void anhxa()
    {
        edit_sp = (EditText) findViewById(R.id.txt_specialist);
        btn_sp = (Button) findViewById(R.id.btn_specialist);
    }
}
