package com.example.zahuandinh.do_an_2_2;

import android.app.Dialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class list_doctor extends AppCompatActivity {
ListView listView;
TextView txt_dialog;
ArrayList<String> arrayList;
ArrayAdapter arrayAdapter;
String node="", doctor_name, mode="";
Button return_but, btn_yes,btn_no;
Intent intent;
Dialog dialog;
int vitri_batdau, vitri_ketthuc,tt = 0;
String mail="";
DatabaseReference mData = FirebaseDatabase.getInstance().getReference();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_doctor);
        anhxa();
        intent = getIntent();
        mode = intent.getStringExtra("mode");
        final Animation blink = AnimationUtils.loadAnimation(this,R.anim.blink);
        arrayList = new ArrayList<>();
        arrayAdapter = new ArrayAdapter(list_doctor.this, android.R.layout.simple_list_item_1, arrayList);
        mData.child("USERS").addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                node = dataSnapshot.getValue().toString();
                if(node.length() > 5)
                {
                    if(mode.equals("2"))
                    {
                        vitri_batdau = node.indexOf("quyen=2");
                    }
                    if(mode.equals("1"))
                    {
                        vitri_batdau = node.indexOf("quyen=1");
                    }
                    if(mode.equals("0"))
                    {
                        vitri_batdau = node.indexOf("quyen=0");
                    }
                    if(vitri_batdau > 0)
                    {
                        if(mode.equals("1") || mode.equals("2"))
                        {
                            tt = 1;
                            vitri_batdau = node.indexOf("name_signup=");
                            vitri_ketthuc = node.indexOf(", email_signup");
                            doctor_name = node.substring(vitri_batdau + 12, vitri_ketthuc);
                            vitri_batdau = node.indexOf("email_signup=");
                            vitri_ketthuc = node.indexOf("}");
                            doctor_name = doctor_name + "\n\t\tEmail: " + node.substring(vitri_batdau + 13, vitri_ketthuc);
                            vitri_batdau = node.indexOf("dob_signup=");
                            vitri_ketthuc = node.indexOf(", password_signup=");
                            doctor_name = doctor_name + "\n\t\tBirthday: " + node.substring(vitri_batdau + 11, vitri_ketthuc);
                            vitri_batdau = node.indexOf("gender_signup=");
                            vitri_ketthuc = node.indexOf(", name_signup");
                            doctor_name = doctor_name + "\n\t\tGender: " + node.substring(vitri_batdau + 14, vitri_ketthuc);
                            Log.d("huanzadinh", "doctor: " + doctor_name);
                            arrayList.add(doctor_name);
                            listView.setAdapter(arrayAdapter);
                        }
                        if(mode.equals("0"))
                        {
                            String phat;
                            vitri_batdau = node.indexOf("phat=");
                            phat = node.substring(vitri_batdau+5, vitri_batdau+6);
                            if(phat.equals("3"))
                            {
                                vitri_batdau = node.indexOf("name_signup=");
                                vitri_ketthuc = node.indexOf(", phat");
                                doctor_name = node.substring(vitri_batdau+12, vitri_ketthuc);
                                Log.d("phat_tien_1", "dr 1 : " + doctor_name);
                                vitri_batdau = node.indexOf("email_signup=");
                                vitri_ketthuc = node.lastIndexOf("}");
                                mail = node.substring(vitri_batdau+13, vitri_ketthuc);
                                doctor_name = doctor_name + "\n" + node.substring(vitri_batdau+13, vitri_ketthuc);
                                Log.d("phat_tien_1", "dr 2 : " + doctor_name);
                                arrayList.add(doctor_name);
                                listView.setAdapter(arrayAdapter);
                            }

                        }

                    }
                }
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        return_but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                return_but.startAnimation(blink);
                Intent intent = new Intent(list_doctor.this, admin.class);
                startActivity(intent);
                overridePendingTransition(R.anim.anim_enter, R.anim.amin_out);
                finish();
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                dialog = new Dialog(list_doctor.this);
                dialog.setTitle("Add Room");
                dialog.setContentView(R.layout.yesno);
                txt_dialog = (TextView) dialog.findViewById(R.id.topic);
                txt_dialog.setText("Unlocking this Patient ?");
                btn_yes = (Button) dialog.findViewById(R.id.yes);
                btn_no = (Button) dialog.findViewById(R.id.no);
                dialog.show();
                btn_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        vitri_batdau = mail.indexOf("@");
                        vitri_ketthuc = mail.indexOf(".");
                        mail = mail.substring(0,vitri_batdau) + mail.substring(vitri_batdau+1, vitri_ketthuc);
                        mData.child("USERS").child(mail).child("phat").setValue(0);
                        Toast.makeText(list_doctor.this, "Successful ", Toast.LENGTH_LONG).show();
                        dialog.cancel();
                        finish();
                    }
                });
                btn_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
                return false;
            }
        });
    }
    void anhxa()
    {
        return_but = (Button) findViewById( R.id.return_list_doctor);
        listView = (ListView) findViewById(R.id.doctor_lv);
    }
}
