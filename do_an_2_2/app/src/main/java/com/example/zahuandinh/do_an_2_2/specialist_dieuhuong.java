package com.example.zahuandinh.do_an_2_2;

import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class specialist_dieuhuong {
    public static void dieuhuong(final String email, final String today, final String room)
    {
        final DatabaseReference mData = FirebaseDatabase.getInstance().getReference();
        mData.child("specialist").child(room).child("total_number").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists())
                {
                    int total_number = ((Long) dataSnapshot.getValue()).intValue();
                    total_number++;
                    mData.child("specialist").child(room).child("schedule").child(today).child(String.valueOf(total_number)).setValue(email);
                    mData.child("specialist").child(room).child("total_number").setValue(total_number);
                }
                else
                {
                    Log.d("specialist_dieuhuong", "total_number null");
                    return;
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
