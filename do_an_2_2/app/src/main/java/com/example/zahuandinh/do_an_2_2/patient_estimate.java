package com.example.zahuandinh.do_an_2_2;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class patient_estimate {
    static DatabaseReference mData = FirebaseDatabase.getInstance().getReference();
    public static void estimate(String email, final int room, final int check_number, final int flag)
    {
        // LẤY W_HOUR
        mData.child("ROOM").child(String.valueOf(room)).child("w_hour").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final int[] w_hour = new int[1];
                final double w_wait;
                Log.d("time_estimate", "room : " + room);
                Log.d("time_estimate", "check_number : " + check_number);
                Log.d("time_estimate", "flag : " + flag);
                if(dataSnapshot.exists())
                {
                    w_hour[0] = ((Long) dataSnapshot.getValue()).intValue();
                    // LẤY W_MINUTE
                    mData.child("ROOM").child(String.valueOf(room)).child("w_minute").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists())
                            {
                                final int[] w_minute = new int[1];
                                w_minute[0] =  ((Long) dataSnapshot.getValue()).intValue();
                                mData.child("ROOM").child(String.valueOf(room)).child("w_checknum").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if(dataSnapshot.exists())
                                        {
                                            final int w_checknum;
                                            w_checknum = ((Long) dataSnapshot.getValue()).intValue();
                                            Log.d("time_estimate", "w_checknum : " + w_checknum);
                                            mData.child("ROOM").child(String.valueOf(room)).child("w_wait").addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                    if (dataSnapshot.exists())
                                                    {
                                                        final double w_wait = (double) dataSnapshot.getValue();
                                                        double estimate;
                                                        if(check_number > w_checknum)
                                                        {
                                                            estimate = w_wait*(check_number - w_checknum);
                                                            Log.d("time_estimate", "estimate : " + estimate);
                                                            int hour, minute;
                                                            hour = (int)estimate/60;
                                                            Log.d("time_estimate", "HOUR : " + hour);
                                                            minute = (int)estimate%60;
                                                            Log.d("time_estimate", "MINUTE : " + minute);
                                                            w_hour[0] = w_hour[0] + hour;
                                                            w_minute[0] = w_minute[0] + minute;
                                                            Log.d("time_estimate", "1  hour : " + w_hour[0] + " minute : " + w_minute[0]);
                                                            if(w_minute[0] >= 60)
                                                            {
                                                                w_hour[0] = w_hour[0] + w_minute[0] / 60;
                                                                w_minute[0] = w_minute[0] % 60;
                                                            }
                                                            Log.d("time_estimate", "2  hour : " + w_hour[0] + " minute : " + w_minute[0]);
                                                            if((w_hour[0] > 11) || (w_hour[0] == 11 && w_minute[0] >= 30) )
                                                            {
                                                                w_hour[0] = 13 + (w_hour[0] - 11);
                                                                // Với trường hợp 14h6 thì sau khi tính phần vượt quá giờ rồi cộng vs 13h0 thì sẽ ra 16h -24p nên t phải +60 vô phần phút và giảm 1 ở phần giờ nếu w_minute < 30
                                                                if(w_minute[0] < 30) {
                                                                    w_minute[0] = (w_minute[0] - 30) + 60;
                                                                    w_hour[0] = w_hour[0] -1;
                                                                }
                                                                else {
                                                                    w_minute[0] = (w_minute[0] - 30);
                                                                }
                                                            }
//                                                            else if( )
//                                                            {
//                                                                w_hour[0] = 13 + (w_hour[0] - 11);
//                                                                w_minute[0] = 0 + (w_minute[0] - 30);
//                                                            }
                                                            Log.d("time_estimate", "3  hour : " + w_hour[0] + " minute : " + w_minute[0]);
                                                            if(flag == 1)
                                                            {
                                                                mData.child("ROOM").child(String.valueOf(room)).child("flag").setValue(0);
                                                            }
                                                            tab2_getnumber.txt_appointment.setText("Your Appointment at " + w_hour[0] + " : " + w_minute[0]);
                                                            tab2_getnumber.txt_appointment.setTextColor(Color.parseColor("#DF0029"));
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Log.d("uoc_luong", "w_wait null");
                                                        return;
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                                }
                                            });
                                        }
                                        else
                                        {
                                            Log.d("uoc_luong", "w_checknum null");
                                            return;
                                        }
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            }
                            else
                            {
                                Log.d("uoc_luong", "w_minute null");
                                return;
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
                else {
                    Log.d("uoc_luong", "w_hour null");
                    return;
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
