package com.example.zahuandinh.do_an_2_2;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class setting_infor extends AppCompatActivity {
EditText name_txt, dob_txt, ident_txt;
Button ok_btn;
Intent intent;
String node="";
DatabaseReference mData = FirebaseDatabase.getInstance().getReference();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_infor);
        anhxa();
        intent = getIntent();
        node = intent.getStringExtra("node");
        mData.child("USERS").child(node).child("name_signup").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                name_txt.setText(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        mData.child("USERS").child(node).child("dob_signup").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                dob_txt.setText(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        mData.child("USERS").child(node).child("ident_signup").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                ident_txt.setText(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mData.child("USERS").child(node).child("name_signup").setValue(name_txt.getText().toString());
                mData.child("USERS").child(node).child("dob_signup").setValue(dob_txt.getText().toString());
                mData.child("USERS").child(node).child("ident_signup").setValue(ident_txt.getText().toString());
                Toast.makeText(setting_infor.this, "Update Informations Successfully", Toast.LENGTH_LONG).show();
            }
        });
    }
    void anhxa()
    {
        name_txt = (EditText) findViewById(R.id.name);
        dob_txt = (EditText) findViewById(R.id.birthday);
        ident_txt = (EditText) findViewById(R.id.ident);
        ok_btn = (Button) findViewById(R.id.ok_btn);
    }
}
