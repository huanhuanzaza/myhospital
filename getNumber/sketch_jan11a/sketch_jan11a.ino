#include<ESP8266WiFi.h>
#include <FirebaseArduino.h>
#include <ctime>
// thư viện lấy thời gian thực từ internet src: https://ngoinhaiot.com/lay-thoi-gian-thuc-tu-internet-bang-esp8266/?fbclid=IwAR0YmJEzl7tNmy_5uc-wvJanjrL0zBPnH8-QIXb87alQqXuvo52cf5HxoU8
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
// LINK OF FIREBASE
#define FIREBASE_HOST "myhospital-4f3a6.firebaseio.com"
#define FIREBASE_AUTH ""
// SSID AND PASSWORD OF WIFI
#define WIFI_SSID "Cafe goc 1"
#define WIFI_PASSWORD "123456789"
// BIẾN
int soluong, nextroom,total_number,i=0, tt, yy, mm, dd, soluong_schedule = 0, room_mode=0, sieuam_number, xray_number, noisoi_number, phauthuat_number, check_total;
String room;
String dateFirebase;
String dateSystem;
String date_temp="";
byte vitri;
boolean check=true;
WiFiUDP u;
NTPClient n(u,"3.asia.pool.ntp.org",3600*7);
LiquidCrystal_I2C lcd(0x27, 16, 2);
// HÀM KHỞI TẠO - CHẠY MỘT LẦN KHI KHỞI ĐỘNG
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  
  // KẾT NỐI VỚI WIFI
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("connecting");
  while (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(500);
  }
  Serial.println();
  Serial.print("Connected: ");
  Serial.println(WiFi.localIP());
  n.begin();
  //connect to Firebase
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  delay(300);
  //KHỞI TẠO LCD
  lcd.begin(20,4);
  lcd.init();
  // Turn on the blacklight and print a message.
  lcd.backlight();
  lcd.clear();
  lcd.setCursor(4,1);
  lcd.print("MY HOSPITAL");
  // KHAI BÁO NGÕ INPUT/ OUTPUT
  // CHÂN 16 LÀ NÚT NHẤN GET NUMBER
  pinMode(16,INPUT);
  // CHÂN 2 LÀ NÚT NHẤN CHUYỂN CHẾ ĐỘ XEM
  pinMode(2,INPUT);
  // CHÂN SỐ 0 LÀ BUZZER
  pinMode(0,OUTPUT);
  // LẤY GIÁ TRỊ CẦN THIẾT TỪ FIREBASE
  // SỐ LƯỢNG PHÒNG KHÁM HIỆN CÓ
  soluong = Firebase.getInt("ROOM/soluong");
  Serial.println(soluong);

  //LẤY THÔNG TIN NGÀY THÁNG HIỆN HỮU Ở FIREBASE ĐỂ CẬP NHẬT DỮ LIỆU
  dateFirebase = Firebase.getString("date");
  date_temp = dateFirebase;
  Serial.println("DATE FIREBASE: " + dateFirebase);
  //LẤY THÔNG TIN THỜI GIAN (NĂM -THÁNG- NGÀY- THỜI GIAN) CỦA HỆ THỐNG 
  n.update();
  delay(500);
  n.update();
  // LẤY NGÀY THÁNG NĂM TỪ CHUỖI NHẬN ĐƯỢC -->2019-05-05T11:24:55Z
  dateSystem = n.getFormattedDate();
  vitri = dateSystem.indexOf("T");
  dateSystem = dateSystem.substring(0,vitri);
  //Có một vài trường hợp Node MCU không cập nhật ngày hiện tại, ta khắc chế bằng cách cho Node Chạy lại.
  if(dateSystem == "1970-01-01")
  {
    Serial.println("**** Reser Node do date : 1970-01-01");
    return;
  }
  Serial.println("today s: " +  dateSystem);
  // THÔNG TIN CÓ ĐỊNH DẠNG: NĂM-THÁNG-NGÀY
  // CHUYỂN ĐỊNH DẠNG THÀNH NGÀY-THÁNG-NĂM ĐỂ SO SÁNH
  vitri = dateSystem.lastIndexOf("-");
  //Loại bỏ số 0 ở đầu mỗi tháng 03 02 01 05 04
  String mm = dateSystem.substring(dateSystem.indexOf("-")+1,vitri);
  if(mm.substring(0,1) == "0")
  {
    mm = mm.substring(1);
    Serial.println("mm: " +  mm);
  }
  //Loại bỏ số 0 ở đầu mỗi tháng 03 02 01 05 04
  String dd = dateSystem.substring(vitri+1);
  if(dd.substring(0,1) == "0")
  {
    dd = dd.substring(1);
    Serial.println("dd: " +  dd);
  }
  date_temp = dd + "-" + mm + "-" + dateSystem.substring(0,dateSystem.indexOf("-"));
  dateSystem = date_temp;
  Serial.println("today: " +  date_temp);
  // KIỂM TRA TRẠNG THÁI CẬP  NHẬT DỮ LIỆU CỦA FIREBASE
  // NẾU NGÀY CỦA FIREBASE TRÙNG VS NGÀY CỦA SYSTEM TRÙNG --> ĐÃ CẬP NHẬT <> NGƯỢC LẠI THÌ CHƯA CẬP NHẬT --> TIẾN HÀNH CẬP NHẬT
  if(date_temp == dateFirebase)
  {
    Serial.println("ALREADY UPDATED");
  }
  else
  {
    // CHƯA CẬP NHẬT DỮ LIỆU
    Serial.println("NOT UPDATE YET");
    Serial.println("today s: " +  dateSystem);
    soluong_schedule = Firebase.getInt("SCHEDULE/" + date_temp + "/soluong");
    //KIỂM TRA SỐ LƯỢNG BỆNH NHÂN BOOK SỐ KHÁM CHO NGÀY HÔM NAY
    // KHÔNG CÓ NGƯỜI ĐẶT KHÁM CHO HÔM NAY
    if(soluong_schedule == 0)
    {
       Serial.println("\nNO BOOKING TODAY");
       for(int i = 1; i <= soluong; i++)
       {
          Firebase.setInt("ROOM/" + (String)i + "/w_shift",0);
          Firebase.setInt("ROOM/" + (String)i + "/w_minute",0);
          Firebase.setInt("ROOM/" + (String)i + "/w_hour",6);
          Firebase.setInt("ROOM/" + (String)i + "/check_number",0);
          Firebase.setInt("ROOM/" + (String)i + "/total_number",0);
          Firebase.setInt("ROOM/" + (String)i + "/flag",0);
          Firebase.setInt("ROOM/" + (String)i + "/w_checknum",0);
          
       }
       Firebase.setInt("ROOM/nextroom",1);
       
    }
    // CÓ NGƯỜI ĐẶT KHÁM CHO HÔM NAY
    else
    {
       Serial.println("UPDATING BOOKING...................");
       for(int i = 1; i <= soluong_schedule; i++)
       {
          Firebase.setInt("ROOM/" + (String)i + "/flag",0);
          Firebase.setInt("ROOM/" + (String)i + "/w_checknum",0);
          Firebase.setInt("ROOM/" + (String)i + "/w_shift",0);
          Firebase.setInt("ROOM/" + (String)i + "/w_minute",0);
          Firebase.setInt("ROOM/" + (String)i + "/w_hour",6);
          total_number = Firebase.getInt("SCHEDULE/" + date_temp + "/" + (String)i + "/" + "total_number");
          Firebase.setInt("ROOM/" + (String)i + "/total_number",total_number);
          Firebase.setInt("ROOM/" + (String)i + "/check_number",0);
          nextroom = Firebase.getInt("SCHEDULE/" + dateSystem + "/" + "nextroom");
          Firebase.setInt("ROOM/nextroom",nextroom);
       }
       for(int i = soluong_schedule+1; i <= soluong; i++ )
       {
          Firebase.setInt("ROOM/" + (String)i + "/w_shift",0);
          Firebase.setInt("ROOM/" + (String)i + "/w_minute",0);
          Firebase.setInt("ROOM/" + (String)i + "/w_hour",6);
          Firebase.setInt("ROOM/" + (String)i + "/check_number",0);
          Firebase.setInt("ROOM/" + (String)i + "/total_number",0);
          Firebase.setInt("ROOM/" + (String)i + "/flag",0);
          Firebase.setInt("ROOM/" + (String)i + "/w_checknum",0);
       }
    }
    Firebase.setString("date",date_temp);
    Serial.println("\n============== COMPLETED ==============\ndateUpdate : " + date_temp);
  }
  //BÁO HIỆU ĐÃ HOÀN TẤT TẢI DỮ LIỆU
  // SET GIÁ TRỊ BAN ĐẦU
  digitalWrite(0,HIGH);
  delay(30);
  digitalWrite(0,LOW);
  delay(30);
  digitalWrite(0,HIGH);
  delay(30);
  digitalWrite(0,LOW);
  lcd.clear();
  lcd.setCursor(0,3);
  lcd.print(" === GET NUMBER ===");
}

// VÒNG LẶP 
void loop() {
  
  // put your main code here, to run repeatedly:
  check = true;
  if(digitalRead(2) == 0)
  {
    delay(10);
    if(digitalRead(2) == 0)
    {
        lcd.clear();
        lcd.setCursor(0,0);
        room_mode++;
        if(room_mode == 5)
        {
            room_mode = 0;
        }
        switch(room_mode)
        {
          case 0:
          {
            lcd.print("------Admisson------");
            break;
          }
          case 1:
          {
            lcd.print("------Sieu.Am-------");
            break;
          }
          case 2:
          {
            lcd.print("--------X.Ray-------");
            break;
          }
          case 3:
          {
            lcd.print("------Noi.Soi-------");
            break;
          }
          case 4:
          {
            lcd.print("-----Phau.Thuat-----");
            break;
          }
        }
        Serial.print("hello\n");
        Serial.print("room_mode : " + (String) room_mode);
        digitalWrite(0,HIGH);
        delay(30);
        digitalWrite(0,LOW);
    }
    while(digitalRead(2) == 0);
  }
  
  if(digitalRead(16) == 0)
  {
    delay(10);
    if(digitalRead(16) == 0)
    {
      if(room_mode == 0)
      {
          //Lấy thông tin phòng khám sẽ được cấp số kế tiếp
          nextroom = Firebase.getInt("ROOM/nextroom");
          //Xử lý trường hợp dữ liệu nhận về bị lỗi nextroom = 0 ; gây ảnh hưởng chương trình --> set nextroom = 1;
          if(nextroom == 0)
          {
              Serial.print("****** NEXTROOM = 0 *******\n");
              lcd.clear();
              lcd.setCursor(0,1);
              lcd.print("   PROCESSING...    ");
              return;
          }
          // Lấy dữ liệu về số lượng bệnh nhân của phòng khám
          room = (String) nextroom;
          if(total_number == 0)
          {
            check_total = 0;
          }
          else
          {
            check_total = 1;
          }
          total_number = Firebase.getInt("ROOM/" + room + "/total_number");
          if(total_number == 0 && check_total == 1)
          {
              Serial.print("****** TOTAL = 0 *******\n");
              lcd.clear();
              lcd.setCursor(0,1);
              lcd.print("   PROCESSING...    ");
              return;
          }
          
          else
          {
            Firebase.setInt("ROOM/" + room + "/total_number",total_number+1);
            //KIỂM TRA VIỆC CẬP NHẬT DỮ LIỆU LÊN FIREBASE CÓ THÀNH CÔNG HAY KHÔNG
            if (Firebase.failed()) {
                Serial.print("setting /number failed:");
                Serial.println(Firebase.error());  
                return;
            }
            // Hiển thị thông tin đã cấp phát
            int a = total_number + 1;
            int room_s = nextroom;
            Serial.print("Your number: " + (String)a + " At Room: " + (String) room_s + "\n");
            lcd.clear();
            lcd.setCursor(0,0);
            lcd.print("------Admisson------");
            lcd.setCursor(0,1);
            lcd.print(" Your Number: " + (String)a + "     ------------------");
            lcd.setCursor(0,2);
            lcd.print(" At Room: " + (String) room_s + "   ");
            // Nếu cấp phát tới phòng cuối thì set nextroom = 1
            if(nextroom >= soluong)
            {
                Firebase.setInt("ROOM/nextroom",1);
                Serial.println("NEXT ROOM : 1\n");
            }
            else
            {
                Firebase.setInt("ROOM/nextroom", nextroom+1);
                Serial.println("NEXT ROOM : " + (String)(nextroom+1) + "\n");
            }
          }
      }
      else if(room_mode == 1)
      {
          if(sieuam_number == 0)
          {
            check_total = 0;
          }
          else
          {
            check_total = 1;
          }
          sieuam_number = Firebase.getInt("specialist/Sieuam/total_number");
          if(sieuam_number == 0 && check_total == 1)
          {
            lcd.setCursor(0,1);
            lcd.print("   PROCESSING...    ");
            lcd.setCursor(0,2);
            lcd.print("                    ");
            return;
          }
          sieuam_number = sieuam_number + 1;
          lcd.setCursor(0,1);
          lcd.print(" Your Number: " + (String)sieuam_number + "     ------------------");
          lcd.setCursor(0,2);
          lcd.print(" At Room: SieuAm   ");
          Firebase.setInt("specialist/Sieuam/total_number", sieuam_number);
          Serial.println("Sieu Am : " + (String)sieuam_number + "\n");
      }
      else if(room_mode == 2)
      {
          if(xray_number == 0)
          {
            check_total = 0;
          }
          else
          {
            check_total = 1;
          }
          xray_number = Firebase.getInt("specialist/X-Ray/total_number");
          if(xray_number == 0 && check_total == 1)
          {
            lcd.setCursor(0,1);
            lcd.print("   PROCESSING...    ");
            lcd.setCursor(0,2);
            lcd.print("                    ");
            return;
          }
          xray_number = xray_number + 1;
          lcd.setCursor(0,1);
          lcd.print(" Your Number: " + (String)xray_number + "     ------------------");
          lcd.setCursor(0,2);
          lcd.print(" At Room: X-Ray   ");
          Firebase.setInt("specialist/X-Ray/total_number", xray_number);
          Serial.println("X-Ray : " + (String)xray_number + "\n");
      }
      else if(room_mode == 3)
      {
          if(noisoi_number == 0)
          {
            check_total = 0;
          }
          else
          {
            check_total = 1;
          }
          noisoi_number = Firebase.getInt("specialist/noisoi/total_number");
          if(noisoi_number == 0 && check_total == 1)
          {
            lcd.setCursor(0,1);
            lcd.print("   PROCESSING...    ");
            lcd.setCursor(0,2);
            lcd.print("                    ");
            return;
          }
          noisoi_number = noisoi_number + 1;
          lcd.setCursor(0,1);
          lcd.print(" Your Number: " + (String)noisoi_number + "     ------------------");
          lcd.setCursor(0,2);
          lcd.print(" At Room: Noi Soi   ");
          Firebase.setInt("specialist/noisoi/total_number", noisoi_number);
          Serial.println("Noi Soi : " + (String)noisoi_number + "\n");
      }
      else if(room_mode == 4)
      {
          if(phauthuat_number == 0)
          {
            check_total = 0;
          }
          else
          {
            check_total = 1;
          }
          phauthuat_number = Firebase.getInt("specialist/phauthuat/total_number");
          if(phauthuat_number == 0 && check_total == 1)
          {
            lcd.setCursor(0,1);
            lcd.print("   PROCESSING...    ");
            lcd.setCursor(0,2);
            lcd.print("                    ");
            return;
          }
          phauthuat_number = phauthuat_number + 1;
          lcd.setCursor(0,1);
          lcd.print(" Your Number: " + (String)phauthuat_number + "     ------------------");
          lcd.setCursor(0,2);
          lcd.print(" At Room: Phau Thuat");
          Firebase.setInt("specialist/phauthuat/total_number", phauthuat_number);
          Serial.println("Phau Thuat : " + (String)phauthuat_number + "\n");
      }
       // Cho BUZZER kêu khi hoàn thành
       digitalWrite(0,HIGH);
       delay(40);
       digitalWrite(0,LOW);
    }
    while(digitalRead(16) == 0);
  }
}
